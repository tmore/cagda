module HUnit.Spec where


import Test.HUnit

import qualified HUnit.C.Token.Spec as Token

runTests = do
  runTestTT (TestList [ Token.tests
                      ])
