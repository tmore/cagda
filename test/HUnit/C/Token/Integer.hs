-- | Author: Tomas Möre 2019
{-# LANGUAGE FlexibleContexts #-}

module HUnit.C.Token.Integer where

import Text.Megaparsec

import Test.HUnit
import HUnit.Util

import C.Token.Integer

-- prop_integerConstantTokenStringRules ::
tests =
  TestList [ test_succesfullIntegerSuffixes
           , test_badIntegerSuffixes
           , test_generalInteger
           ]

test_succesfullIntegerSuffixes =
  assertParseListEquals (optional (try integerSuffix)) (("", Nothing) : cases)
  where
    cases = map (\ (s,r) -> (s, Just (r s)))
            [ ("u", Int_U)
            ,("U", Int_U)
            ,("l", Int_L)
            ,("L", Int_L)
            ,("ul", Int_UL)
            ,("Ul", Int_UL)
            ,("UL", Int_UL)

            ,("ll", Int_LL)
            ,("lL", Int_LL)
            ,("Ll", Int_LL)
            ,("lL", Int_LL)
            ,("LL", Int_LL)

            ,("ull", Int_ULL)
            ,("Ull", Int_ULL)
            ,("ULl", Int_ULL)
            ,("uLl", Int_ULL)
            ,("uLl", Int_ULL)
            ,("llu", Int_LLU)
            ]
test_badIntegerSuffixes =
  assertParseListFails (integerSuffix <* eof) cases
  where
    cases :: [String]
    cases = [""
            ,"lul"
            ,"uz"
            ,"lla"
            ,"lal"
            ,"ullz"
            ,"llk"
            ,"ula"
            ]

test_generalInteger =
  assertParseListEquals integerConstant cases
  where
    cases = [ ("123", (DecIC (DecC "123") Nothing))
            , ("922uL", (DecIC (DecC "922") (Just (Int_UL "uL"))))
            , ("1337Ull", (DecIC (DecC "1337") (Just (Int_ULL "Ull"))))
            , ("0xFEE1DEAD", (HexIC (HexC Hex0x (HexDSeq "FEE1DEAD")) Nothing))
            , ("0XFEE1DEADl", (HexIC (HexC Hex0X (HexDSeq "FEE1DEAD")) (Just (Int_L "l"))))
            , ("01772", (OctIC (OctC "1772") Nothing))
            , ("0", (OctIC (OctC "") Nothing))
            , ("0l", (OctIC (OctC "") (Just (Int_L "l"))))
            ]
