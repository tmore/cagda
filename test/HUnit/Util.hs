-- | Author: Tomas Möre

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
module HUnit.Util where

import Text.Megaparsec
import Test.HUnit

import Data.Functor.Identity

type Debuggable e s a = (Eq a, Show a
                        , Show s, Show (Token s), Stream s
                        , Show e)

assertParseFails :: Debuggable e s a => s -> ParsecT e s Identity a -> Assertion
assertParseFails source parser =
  case parse (parser) "TEST_SOURCE" source of
    Left _ ->
      pure ()
    Right res ->
      assertFailure $ concat [ "Parser expected to fail but gave a result "
                             , show res
                             ," "
                             , "On input: "
                             , show source
                             ]

assertParseEquals :: Debuggable e s a => s -> ParsecT e s Identity a -> a -> Assertion
assertParseEquals source parser match =
  case parse parser "TEST_SOURCE" source of
    Left err ->
      assertFailure $ concat [ "Parser failed with error message: "
                             , show err
                             , " Expected "
                             , show match
                             ]
    Right parseRes ->
      if parseRes == match
      then pure ()
      else assertFailure $ concat [ "Parser results did not match"
                                 , " expected ", show match
                                 , " but got ", show parseRes
                                 ]


assertParseSatisfies :: Debuggable e s a => s -> ParsecT e s Identity a -> (a -> Bool) -> Assertion
assertParseSatisfies source parser predicate =
  case parse parser "TEST_SOURCE" source of
    Left err ->
      assertFailure $ concat [ "Parser failed with error message: "
                             , show err
                             ]
    Right parseRes ->
      if predicate parseRes
      then pure ()
      else assertFailure $ concat [ "Parser results did not satisfy the predicate"
                                  , " got "
                                  , show parseRes
                                  ]

-- | Runs the parser on a list of tuples (s,a) where s in the string to be
-- parsed and a is the expected output. If the result of the paser fails or gives something else the test fails.
assertParseListEquals :: Debuggable e s a => ParsecT e s Identity a -> [(s, a)] -> Test
assertParseListEquals parser assocList =
  TestList $ fmap toTestCase assocList
  where
    toTestCase (s,match) =
      TestCase $ assertParseEquals s parser match

-- | Runs the parser on a list of input that all are expected to fail
assertParseListFails :: Debuggable e s a => ParsecT e s Identity a -> [s] -> Test
assertParseListFails parser assocList =
  TestList $ fmap toTestCase assocList
  where
    toTestCase s =
      TestCase $ assertParseFails s parser
