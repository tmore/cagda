module QC.Tests where

import Test.QuickCheck

import QC.C.Token.AllTests (allTokenProperties)

runTests :: IO ()
runTests = do
  mapM_ checkProp allTokenProperties
  pure ()

  where
    checkProp (name, prop) = do
      putStr "Checkin property: "
      putStrLn name
      quickCheck prop
      putStrLn ""
