-- | Author: Tomas Möre 2019
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiWayIf #-}
module QC.C.Token.String where

import QC.C.Token.TokenString
import QC.C.Token.TokenGen
import QC.C.Token.Character

import Test.QuickCheck

-- Some arbitrary instances come from here
import C.Token.Util
import C.Token.String
import C.Token.Character

import Data.List ((\\))

prop_StringLitteral_TokenStringProp1 :: StringLitteral -> Bool
prop_StringLitteral_TokenStringProp1 = tokenStringProp1

prop_StringLitteral_TokenStringProp2  =
  forAll arbStringLiteral (tokenStringProp2 @StringLitteral)

instance Arbitrary StringLitteral where
  arbitrary =
    StringLit <$> arbitrary <*> arbitrary


prop_SChar_TokenStringProp1 :: SChar -> Bool
prop_SChar_TokenStringProp1 = tokenStringProp1

prop_SChar_TokenStringProp2  =
  forAll arbSChar (tokenStringProp2 @SChar)

instance Arbitrary SChar where
  arbitrary =
    oneof [ SimpleSC <$> (elements (sourceCharacterSet \\ ['\\','\"', '\n','\r'])  )
          , EscapeSC <$> arbitrary
          ]

prop_SCharSequence_TokenStringProp1 :: SCharSequence -> Bool
prop_SCharSequence_TokenStringProp1 = tokenStringProp1

prop_SCharSequence_TokenStringProp2  =
  forAll arbSCharSequence (tokenStringProp2 @SCharSequence)

instance Arbitrary SCharSequence where
  arbitrary =
    SCharSequence <$> listOf arbitrary


prop_EncodingPrefix_TokenStringProp1 :: EncodingPrefix -> Bool
prop_EncodingPrefix_TokenStringProp1 = tokenStringProp1

prop_EncodingPrefix_TokenStringProp2 =
  forAll arbEncodingPrefix (tokenStringProp2 @EncodingPrefix)

instance Arbitrary EncodingPrefix where
  arbitrary =
    elements [ Enc_u8
             , Enc_u
             , Enc_U
             , Enc_L
             ]


return[]
allStringProperties = $allProperties



{-
prop_TokenStringProp1 :: _ -> Bool
prop_TokenStringProp1 = tokenStringProp1

prop_TokenStringProp2  =
  forAll _ (tokenStringProp2 @)

instance Arbitrary where
  arbitrary =
-}
