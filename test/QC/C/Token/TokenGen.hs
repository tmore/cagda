-- | Author: Tomas Möre 2019
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiWayIf #-}

module QC.C.Token.TokenGen where

import Test.QuickCheck

import qualified Data.Char as Char

import Data.List ((\\))

import C.Token.Keyword
import C.Token.Util (sourceCharacterSet)
import C.Token.Punctuator (allPunctuators)

import Data.String

-- | ConstraintKind of what a classes a string should bellong to
type Stringlike s = (Monoid s, IsString s, Ord s)

-- | Lifted mappend that works though an applicative instance
aplMappend :: (Applicative m, Monoid a) => m a -> m a -> m a
aplMappend g1 g2 = mappend <$> g1 <*> g2

-- | Local operator for 'lifted mappend'
(<<>>) :: (Applicative m, Monoid a) => m a -> m a -> m a
(<<>>) = aplMappend

infixl 4 <<>>

-- | Chains the applicative mappend over a foldable
aplMconcat :: (Foldable f, Applicative m, Monoid a) => f (m a) -> m a
aplMconcat = foldr aplMappend (pure mempty)

-- | Generates a random string from characters
stringOf :: IsString s => Gen Char -> Gen s
stringOf = fmap fromString . listOf

stringOf1 :: IsString s => Gen Char -> Gen s
stringOf1 = fmap fromString . listOf1

-- | Chooses an arbitrary generator form a list of generators with weithgts
arbGenWeighted :: [(Int, Gen a)] -> Gen a
arbGenWeighted = frequency

-- | Randomly scamles the capitalisation of a string
arbCapitalize :: String -> Gen String
arbCapitalize str =
  mapM f str
  where
    f c = do
      toLower <- arbitrary :: Gen Bool
      let caseSelect =
            if toLower
            then Char.toLower
            else Char.toUpper
      pure $ caseSelect c


-- | Aribrarily uses the genarator or just an empty value. t
arbOpt :: Stringlike s => Int -> Int -> Gen s -> Gen s
arbOpt emptyWeight genWeight gen =
  frequency [ (emptyWeight, pure mempty)
            , (genWeight, gen)
            ]

charToString :: Stringlike s => Char -> s
charToString = fromString . pure

charStringFrom :: Stringlike s => [Char] -> Gen s
charStringFrom = fmap (fromString . pure) . elements
-- The following generators are made to match the defenitions of tokens as close
-- as possible from the spec in order to easily confirm that the match.

-- A.1.2 Keywords

-- | 6.4.1
arbKeyword :: Stringlike s => Gen s
arbKeyword =
  elements keywordList

-- A.1.3 Identifiers

-- | 6.4.2.1
arbIdentifier :: Stringlike s => Gen s
arbIdentifier =
  oneof [ arbIdentifierNondigit
        , arbIdentifier <<>> arbIdentifierNondigit
        , arbIdentifier <<>> arbDigit
        ]
-- | 6.4.2.1
arbIdentifierNondigit :: Stringlike s => Gen s
arbIdentifierNondigit =
  oneof [ arbNonDigit
        , arbUniversalCharacterName
        ]

arbNonDigit :: Stringlike s => Gen s
arbNonDigit =
  fmap charToString $ elements $ ['a' .. 'Z'] ++ ['A'.. 'Z'] ++ ['_']

arbDigit :: Stringlike s => Gen s
arbDigit =
  fmap charToString $ elements ['0' .. '9']

-- A.1.4 Universal character names

-- | 6.4.3
arbUniversalCharacterName :: Stringlike s => Gen s
arbUniversalCharacterName =
  oneof [ mappend "\\u" <$> arbRestrictedHexQuad
        , mappend "\\U" <$> arbHexQuad <<>> arbRestrictedHexQuad
        ]

arbRestrictedHexQuad :: Stringlike s => Gen s
arbRestrictedHexQuad = do
  hq <- aplMconcat [ arbHexadecimalDigit
                   , arbHexadecimalDigit
                   , arbHexadecimalDigit
                   , arbHexadecimalDigit
                   ]
  if | any (hq==) ["0024", "0040", "0060"] -> arbRestrictedHexQuad
     | hq < "00A0" -> arbRestrictedHexQuad
     | "D800" <= hq && hq <= "DFFF" -> arbRestrictedHexQuad
     | otherwise -> pure hq
-- | 6.4.3
arbHexQuad :: Stringlike s => Gen s
arbHexQuad = aplMconcat [ arbHexadecimalDigit
                        , arbHexadecimalDigit
                        , arbHexadecimalDigit
                        , arbHexadecimalDigit
                        ]

-- A.1.5 Constants

-- | 6.4.4
arbConstant :: Stringlike s => Gen s
arbConstant =
  oneof [ arbIntegerConstant
        , arbFloatingConstant

-- doesn't really exist
--        , arbEnumerationConstant
        , arbCharacterConstant
        ]

-- | 6.4.4.1
arbDecimalConstant :: Stringlike s => Gen s
arbDecimalConstant =
  oneof [ arbNonZeroDigit
        , arbDecimalConstant <<>> arbDigit
        ]

-- | 6.4.4.1
arbIntegerConstant :: Stringlike s => Gen s
arbIntegerConstant =
  oneof [ arbDecimalConstant <<>> arbOpt 1 5 arbIntegerSuffix
        , arbOctalConstant <<>> arbOpt 1 5 arbIntegerSuffix
        , arbHexadecimalConstant <<>> arbOpt 1 5 arbIntegerSuffix
        ]

-- | 6.4.4.1
arbOctalConstant :: Stringlike s => Gen s
arbOctalConstant =
  pure "0" <<>> fmap mconcat (listOf arbOctalDigit)

-- | 6.4.4.1
arbHexadecimalConstant :: Stringlike s => Gen s
arbHexadecimalConstant =
  arbHexadecimalPrefix <<>> arbHexadecimalDigitSequence

-- | 6.4.4.1
arbHexadecimalPrefix :: Stringlike s => Gen s
arbHexadecimalPrefix =
  elements ["0x", "0X"]

-- | 6.4.4.1
arbNonZeroDigit :: Stringlike s => Gen s
arbNonZeroDigit =
  charStringFrom  ['1' .. '9']

-- | 6.4.4.1
arbOctalDigit :: Stringlike s => Gen s
arbOctalDigit =
  charStringFrom ['0' .. '7']

-- | 6.4.4.1
arbHexadecimalDigit :: Stringlike s => Gen s
arbHexadecimalDigit =
  charStringFrom $ ['a'.. 'f'] ++ ['A'..'F'] ++ ['0' .. '9']

-- | 6.4.4.1
arbIntegerSuffix :: Stringlike s => Gen s
arbIntegerSuffix =
  oneof [ arbUnsignedSuffix <<>> arbOpt 1 1 arbLongSuffix
        , arbUnsignedSuffix <<>> arbOpt 1 1 arbLongLongSuffix
        , arbLongSuffix <<>> arbOpt 1 1 arbUnsignedSuffix
        , arbLongLongSuffix <<>> arbOpt 1 1 arbUnsignedSuffix
        ]

-- | 6.4.4.1
arbUnsignedSuffix :: Stringlike s => Gen s
arbUnsignedSuffix =
  elements ["u", "U"]

-- | 6.4.4.1
arbLongSuffix :: Stringlike s => Gen s
arbLongSuffix =
  elements ["l", "L"]

-- | 6.4.4.1
arbLongLongSuffix :: Stringlike s => Gen s
arbLongLongSuffix =
  elements ["ll", "LL"]

-- | 6.4.4.2
arbFloatingConstant :: Stringlike s => Gen s
arbFloatingConstant =
  oneof [ arbDecimalFloatingConstant
        , arbHexadecimalFloatingConstant
        ]

-- | 6.4.4.2
arbDecimalFloatingConstant :: Stringlike s => Gen s
arbDecimalFloatingConstant =
  oneof [ aplMconcat [ arbFractionalConstant
                     , arbOpt 1 5 arbExponentPart
                     , arbOpt 1 2 arbFloatingSuffix
                     ]
        , aplMconcat [ arbDigitSequence
                     , arbExponentPart
                     , arbOpt 1 2 arbFloatingSuffix
                     ]
        ]
-- | 6.4.4.2
arbHexadecimalFloatingConstant :: Stringlike s => Gen s
arbHexadecimalFloatingConstant =
  oneof [ aplMconcat [ arbHexadecimalPrefix
                     , arbHexadecimalFractionalContant
                     , arbBinaryExponentPart
                     , arbOpt 1 2 arbFloatingSuffix
                     ]
        , aplMconcat [ arbHexadecimalPrefix
                     , arbHexadecimalDigitSequence
                     , arbBinaryExponentPart
                     , arbOpt 1 2 arbFloatingSuffix
                     ]
        ]

-- | 6.4.4.2
arbFractionalConstant :: Stringlike s => Gen s
arbFractionalConstant =
  oneof [ aplMconcat [arbOpt 1 5 arbDigitSequence
                     , pure "."
                     , arbDigitSequence
                     ]
        , aplMconcat [ arbDigitSequence
                     , pure "."
                     ]
        ]
-- | 6.4.4.2
arbExponentPart :: Stringlike s => Gen s
arbExponentPart =
  arbExponentPrefix <<>> arbOpt 1 3 arbSign <<>> arbDigitSequence

arbExponentPrefix :: Stringlike s => Gen s
arbExponentPrefix =
  elements ["e","E"]

-- | 6.4.4.2
arbSign :: Stringlike s => Gen s
arbSign = elements ["+","-"]

-- | 6.4.4.2

arbDigitSequence :: Stringlike s => Gen s
arbDigitSequence =
  mconcat <$> listOf1 arbDigit

-- | 6.4.4.2
arbHexadecimalFractionalContant :: Stringlike s => Gen s
arbHexadecimalFractionalContant =
  oneof [ aplMconcat [ arbOpt 1 5 arbHexadecimalDigitSequence
                     , pure "."
                     , arbHexadecimalDigitSequence
                     ]
        , aplMconcat [ arbHexadecimalDigitSequence
                     , pure "."
                     ]
        ]

-- | 6.4.4.2
arbBinaryExponentPart :: Stringlike s => Gen s
arbBinaryExponentPart =
  aplMconcat [arbBinaryExponenetPrefix, arbOpt 1 1 arbSign, arbDigitSequence]

arbBinaryExponenetPrefix :: Stringlike s => Gen s
arbBinaryExponenetPrefix = elements ["p", "P"]

-- | 6.4.4.2
arbHexadecimalDigitSequence :: Stringlike s => Gen s
arbHexadecimalDigitSequence =
  mconcat <$> listOf1 arbHexadecimalDigit

-- | 6.4.4.2
arbFloatingSuffix :: Stringlike s => Gen s
arbFloatingSuffix = elements ["f","F", "l", "L"]


-- | 6.4.4.3
arbEnumerationConstant :: Stringlike s => Gen s
arbEnumerationConstant =
  arbIdentifier

-- | 6.4.4.4
arbCharacterConstant :: Stringlike s => Gen s
arbCharacterConstant =
  aplMconcat [pure "'", arbOpt 1 3 arbCharacterPrefix , arbCCharSequence, pure "'"]

arbCharacterPrefix :: Stringlike s => Gen s
arbCharacterPrefix =
  elements ["L", "u", "U"]

-- | 6.4.4.4
arbCCharSequence :: Stringlike s => Gen s
arbCCharSequence =
  oneof [ arbCChar
        , arbCCharSequence <<>> arbCChar
        ]

-- | 6.4.4.4
arbCChar :: Stringlike s => Gen s
arbCChar =
  oneof [ fmap charToString $ elements $ sourceCharacterSet \\ ['\'', '\\', '\n', '\r']
        , arbEscapeSequence
        ]
-- | 6.4.4.4
arbEscapeSequence :: Stringlike s => Gen s
arbEscapeSequence =
  oneof [ arbSimpleEscapeSequence
        , arbOctalEscapeSequence
        , arbHexadecimalEscapeSequence
        , arbUniversalCharacterName
        ]

-- | 6.4.4.4
arbSimpleEscapeSequence :: Stringlike s => Gen s
arbSimpleEscapeSequence =
  elements ["\\'", "\\\"", "\\?", "\\\\", "\\a"
           , "\\b", "\\f", "\\n", "\\r", "\\t", "\\v"]
-- | 6.4.4.4
arbOctalEscapeSequence :: Stringlike s => Gen s
arbOctalEscapeSequence =
  oneof [ pure "\\" <<>> arbOctalDigit
        , pure "\\" <<>> arbOctalDigit <<>> arbOctalDigit
        , pure "\\" <<>> arbOctalDigit <<>> arbOctalDigit <<>> arbOctalDigit
         ]

-- | 6.4.4.4
arbHexadecimalEscapeSequence :: Stringlike s => Gen s
arbHexadecimalEscapeSequence =
  oneof [ pure "\\x" <<>> arbHexadecimalDigit
        , arbHexadecimalEscapeSequence <<>> arbHexadecimalDigit
        ]

-- A1.6 String litterals

-- | 6.4.5
arbStringLiteral :: Stringlike s => Gen s
arbStringLiteral =
  aplMconcat [ arbOpt 5 1 arbEncodingPrefix
             , pure "\""
             , arbOpt 1 20 arbSCharSequence
             , pure "\""
             ]
-- | 6.4.5
arbEncodingPrefix :: Stringlike s => Gen s
arbEncodingPrefix =
  elements ["u8", "u", "U", "L"]

-- | 6.4.5
arbSCharSequence :: Stringlike s => Gen s
arbSCharSequence =
  oneof [ arbSChar
        , arbSCharSequence <<>> arbSChar
        ]

-- | 6.4.5
arbSChar :: Stringlike s => Gen s
arbSChar =
  oneof [ fmap charToString (elements (sourceCharacterSet \\ ['"', '\\', '\n', '\r']))
        , arbEscapeSequence
        ]

-- A1.7 Punctuators
-- | 6.4.6
arbPunctuator :: Stringlike s => Gen s
arbPunctuator =
  elements allPunctuators

-- A.1.8

-- | 6.4.7
arbHeaderName :: Stringlike s => Gen s
arbHeaderName =
  oneof [ aplMconcat [pure "<"
                     , arbHCharSequence
                     , pure ">"
                     ]
        , aplMconcat [pure "\""
                     , arbHCharSequence
                     , pure "\""
                     ]
        ]
-- | 6.4.7
arbHCharSequence :: Stringlike s => Gen s
arbHCharSequence =
  oneof [ arbHChar
        , arbHCharSequence <<>> arbHChar
        ]

-- | 6.4.7
arbHChar :: Stringlike s => Gen s
arbHChar =
  fmap charToString $ elements $ sourceCharacterSet \\ ['>', '\n', '\r']

-- | 6.4.7
arbQCharSequence :: Stringlike s => Gen s
arbQCharSequence =
  oneof [ arbQChar
        , arbQCharSequence <<>> arbQChar
        ]

-- | 6.4.7
arbQChar :: Stringlike s => Gen s
arbQChar =
    (fromString . pure) <$> elements (sourceCharacterSet \\ ['"', '\n', '\r'])

-- A.1.9 PReprocessing numbers
-- | 6.4.8
arbPPNumber :: Stringlike s => Gen s
arbPPNumber =
  oneof [ arbDigit
        , pure "." <<>> arbDigit
        , arbPPNumber <<>> arbDigit
        , arbPPNumber <<>> arbIdentifierNondigit
        , arbPPNumber <<>> pure "e" <<>> arbSign
        , arbPPNumber <<>> pure "E" <<>> arbSign
        , arbPPNumber <<>> pure "p" <<>> arbSign
        , arbPPNumber <<>> pure "P" <<>> arbSign
        , arbPPNumber <<>> pure "."
        ]
