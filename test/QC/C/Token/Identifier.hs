-- | Author: Tomas Möre
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiWayIf #-}
module QC.C.Token.Identifier where

import QC.C.Token.TokenString
import QC.C.Token.TokenGen

-- Some arbitrary instances come from here
import C.Token.Identifier

import Test.QuickCheck

import Data.List ((\\))

prop_Identifier_TokenStringProp1 :: Identifier -> Bool
prop_Identifier_TokenStringProp1 = tokenStringProp1

prop_Identifier_TokenStringProp2  =
  forAll arbIdentifier (tokenStringProp2 @Identifier)

instance Arbitrary Identifier where
  arbitrary =
    Identifier <$> arbIdentifier

return[]
allCharProperties = $allProperties
