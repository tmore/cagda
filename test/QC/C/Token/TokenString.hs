-- | Author: Tomas Möre 2019
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
module QC.C.Token.TokenString where

import C.Token.TokenString

-- | Checks that the first token string property holds.
--
-- forall a   . parseString (toString a) == Just a
--
-- for some a given as a type argument
tokenStringProp1 :: (TokenString a, Eq a) => a -> Bool
tokenStringProp1 a =
  parseString (toString a') == Just a'
  where
    a' = normalize a

-- | Checks that the second TokenString property holds, this version only
-- applies when exact equality of input and output strings can be
-- checked. Otherwise use the one with custom equality.
tokenStringProp2 :: forall a . TokenString a => String -> Bool
tokenStringProp2 s =
  tokenStringProp2Under @a s (==)


-- -- | Property two execpt where the semanics are insensistive to capitalisation
-- tokenStringProp2CaseInsensitive :: forall a . TokenString a => String -> Bool
-- tokenStringProp2CaseInsensitive s =
--   tokenStringProp2Under @a s caseInsesitiveCompare
--   where
--     caseInsesitiveCompare s1 s2 = fmap Char.toUpper s1 == fmap Char.toUpper s2

tokenStringProp2Under :: forall a . TokenString a => String -> (String -> String -> Bool) -> Bool
tokenStringProp2Under s equalityCheck =
  case parsed of
    Nothing -> False
    Just a -> toString a `equalityCheck` s
  where
    parsed = parseString @a s
