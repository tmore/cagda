-- | Author: Tomas Möëe
module QC.C.Token.AllTests where


import QC.C.Token.Integer (allIntegerProperties)
import QC.C.Token.Floating (allFloatingProperties)
import QC.C.Token.Character (allCharProperties)
import QC.C.Token.String (allStringProperties)

import QC.C.Token.Constant (allConstantProperties)

allTokenProperties =
  mconcat [ allIntegerProperties
          , allFloatingProperties
          , allCharProperties
          , allStringProperties
          , allConstantProperties
          ]
