-- | Author: Tomas Möre
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiWayIf #-}
module QC.C.Token.Character where

import QC.C.Token.TokenString
import QC.C.Token.TokenGen

-- Some arbitrary instances come from here
import C.Token.Character
import C.Token.Util

import Test.QuickCheck

import Data.List ((\\))

prop_CharacterConstant_TokenStringProp1 :: CharacterConstant -> Bool
prop_CharacterConstant_TokenStringProp1 = tokenStringProp1

prop_CharacterConstant_TokenStringProp2  =
  forAll arbCharacterConstant (tokenStringProp2 @CharacterConstant)

instance Arbitrary CharacterConstant where
  arbitrary =
    CharConst <$> arbitrary <*> arbitrary

prop_CharPrefix_TokenStringProp1 :: CharPrefix -> Bool
prop_CharPrefix_TokenStringProp1 = tokenStringProp1

prop_CharPrefix_TokenStringProp2  =
  forAll arbCharacterPrefix (tokenStringProp2 @CharPrefix)

instance Arbitrary CharPrefix where
  arbitrary = elements [ WCharCP
                       , Char16CP
                       , Char32CP
                       ]

prop_CCharSequence_TokenStringProp1 :: CCharSequence -> Bool
prop_CCharSequence_TokenStringProp1 = tokenStringProp1

prop_CCharSequence_TokenStringProp2  =
  forAll arbCCharSequence (tokenStringProp2 @CCharSequence)

instance Arbitrary CCharSequence where
  arbitrary = CCharSequence <$> listOf1 arbitrary


prop_CChar_TokenStringProp1 :: CChar -> Bool
prop_CChar_TokenStringProp1 = tokenStringProp1

prop_CChar_TokenStringProp2  =
  forAll arbCChar (tokenStringProp2 @CChar)

instance Arbitrary CChar where
  arbitrary =
    oneof [ SimpleCC <$> elements (sourceCharacterSet \\ ['\\','\'', '\n','\r'])
          , EscapeCC <$> arbitrary
          ]

prop_EscapeSequence_TokenStringProp1 :: EscapeSequence -> Bool
prop_EscapeSequence_TokenStringProp1 = tokenStringProp1

prop_EscapeSequence_TokenStringProp2  =
  forAll arbEscapeSequence (tokenStringProp2 @EscapeSequence)

instance Arbitrary EscapeSequence where
  arbitrary =
    oneof [ SimpleES <$> arbitrary
          , OctES <$> arbitrary
          , HexES <$> arbitrary
          ]


prop_SimpleEscapeSeq_TokenStringProp1 :: SimpleEscapeSequence -> Bool
prop_SimpleEscapeSeq_TokenStringProp1 = tokenStringProp1

prop_SimpleEscapeSeq_TokenStringProp2  =
  forAll arbSimpleEscapeSequence (tokenStringProp2 @SimpleEscapeSequence)

instance Arbitrary SimpleEscapeSequence where
  arbitrary = SimpleEscapeSequence <$> elements "\'\"?\\abfnrtv"

prop_OctalEscapeSeq_TokenStringProp1 :: OctalEscapeSequence -> Bool
prop_OctalEscapeSeq_TokenStringProp1 = tokenStringProp1

prop_OctalEscapeSeq_TokenStringProp2  =
  forAll arbOctalEscapeSequence (tokenStringProp2 @OctalEscapeSequence)

instance Arbitrary OctalEscapeSequence where
  arbitrary =
    OctalEscapeSequence <$> mconcat <$> fmap (take 3) (listOf1 arbOctalDigit)

prop_HexadecEscapeSeq_TokenStringProp1 :: HexadecEscapeSequence -> Bool
prop_HexadecEscapeSeq_TokenStringProp1 = tokenStringProp1

prop_HexadecEscapeSeq_TokenStringProp2  =
  forAll arbHexadecimalEscapeSequence (tokenStringProp2 @HexadecEscapeSequence)

instance Arbitrary HexadecEscapeSequence where
  arbitrary =
    HexadecEscapeSequence <$> arbHexadecimalDigitSequence


prop_UniversalCharacterName_TokenStringProp1 :: UniversalCharacterName -> Bool
prop_UniversalCharacterName_TokenStringProp1 = tokenStringProp1

prop_UniversalCharacterName_TokenStringProp2  =
  forAll arbUniversalCharacterName (tokenStringProp2 @UniversalCharacterName)

instance Arbitrary UniversalCharacterName where
  arbitrary = do
    oneof [ UCN1 <$> genLeast
          , UCN2 <$> arbitrary <*> genLeast
          ]
      where
        quad0024 = unsafeFromString "0024"
        quad0040 = unsafeFromString "0040"
        quad0060 = unsafeFromString "0060"
        quad00A0 = unsafeFromString "00A0"
        quadD800 = unsafeFromString "D800"
        quadDFFF = unsafeFromString "DFFF"

        unsafeFromString [c1,c2,c3,c4] =
          (HexQuad c1 c2 c3 c4)

        genLeast = do
          hq <- arbitrary
          if | any (hq==) [quad0024, quad0040, quad0060] -> genLeast
             | hq < quad00A0 -> genLeast
             | quadD800 <= hq && hq <= quadDFFF -> genLeast
             | otherwise -> pure hq


prop_HexQuad_TokenStringProp1 :: HexQuad -> Bool
prop_HexQuad_TokenStringProp1 = tokenStringProp1

prop_HexQuad_TokenStringProp2  =
  forAll arbHexQuad (tokenStringProp2 @HexQuad)

instance Arbitrary HexQuad where
  arbitrary =
    HexQuad <$> fmap head arbHexadecimalDigit
            <*> fmap head arbHexadecimalDigit
            <*> fmap head arbHexadecimalDigit
            <*> fmap head arbHexadecimalDigit



return[]
allCharProperties = $allProperties
