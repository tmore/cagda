-- | Author: Tomas Möre 2019
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiWayIf #-}
module QC.C.Token.Constant where

import QC.C.Token.TokenString
import QC.C.Token.TokenGen

import QC.C.Token.Integer()
import QC.C.Token.Floating()
import QC.C.Token.Character()
import QC.C.Token.String()

-- Some arbitrary instances come from here
import C.Token.Constant


import Test.QuickCheck


prop_Constant_TokenStringProp1 :: Constant -> Bool
prop_Constant_TokenStringProp1 = tokenStringProp1

prop_Constant_TokenStringProp2  =
  forAll arbConstant (tokenStringProp2 @Constant)

instance Arbitrary Constant where
  arbitrary =
    oneof [ IntC <$> arbitrary
          , FloatC <$> arbitrary
          , CharC <$> arbitrary
          ]


return[]
allConstantProperties = $allProperties
