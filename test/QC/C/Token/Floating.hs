-- | Author: Tomas Möre
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TemplateHaskell #-}
module QC.C.Token.Floating where

import QC.C.Token.TokenString
import QC.C.Token.TokenGen

-- Some arbitrary instances come from here
import QC.C.Token.Integer

import C.Token.Integer
import C.Token.Floating

import Test.QuickCheck

prop_FloatingConstant_TokenStringProp1 :: FloatingConstant -> Bool
prop_FloatingConstant_TokenStringProp1 = tokenStringProp1

prop_FloatingConstant_TokenStringProp2  =
  forAll arbFloatingConstant (tokenStringProp2 @FloatingConstant)

instance Arbitrary FloatingConstant where
  arbitrary = oneof [ DecFloatC <$> arbitrary
                    , HexFloatC <$> arbitrary
                    ]

prop_DecimalFloatingConstant_TokenStringProp1 :: DecimalFloatingConstant -> Bool
prop_DecimalFloatingConstant_TokenStringProp1 = tokenStringProp1

prop_DecimalFloatingConstant_TokenStringProp2  =
  forAll arbDecimalFloatingConstant (tokenStringProp2 @DecimalFloatingConstant)

instance Arbitrary DecimalFloatingConstant where
  arbitrary = oneof [ FracC <$> arbitrary <*> arbitrary <*> arbitrary
                    , DSeqC <$> arbitrary <*> arbitrary <*> arbitrary
                    ]


prop_HexadecimalFloatingConstant_TokenStringProp1 :: HexadecimalFloatingConstant -> Bool
prop_HexadecimalFloatingConstant_TokenStringProp1 = tokenStringProp1

prop_HexadecimalFloatingConstant_TokenStringProp2  =
  forAll arbHexadecimalFloatingConstant (tokenStringProp2 @HexadecimalFloatingConstant)

instance Arbitrary  HexadecimalFloatingConstant where
  arbitrary = oneof [ HexFracC <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
                    , HexDSeqC <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
                    ]

prop_FractionalConstant_TokenStringProp1 :: FractionalConstant -> Bool
prop_FractionalConstant_TokenStringProp1 = tokenStringProp1

prop_FractionalConstant_TokenStringProp2  =
  forAll arbFractionalConstant (tokenStringProp2 @FractionalConstant)

instance Arbitrary FractionalConstant where
  arbitrary = do
    oneof [ FractionalConstant <$> arbitrary <*> (Just <$> dseq)
          , FractionalConstant <$> (Just <$> dseq) <*> (pure Nothing)
          ]
      where
        dseq = arbitrary :: Gen DigitSequence

prop_ExponentPart_TokenStringProp1 ::  ExponentPart -> Bool
prop_ExponentPart_TokenStringProp1 = tokenStringProp1

prop_ExponentPart_TokenStringProp2 =
  forAll arbExponentPart (tokenStringProp2 @ExponentPart)

instance Arbitrary ExponentPart where
  arbitrary =
    ExponentPart <$> arbitrary <*> arbitrary <*> arbitrary


prop_ExponentPrefix_TokenStringProp1 :: ExponentPrefix -> Bool
prop_ExponentPrefix_TokenStringProp1 = tokenStringProp1

prop_ExponentPrefix_TokenStringProp2  =
  forAll arbExponentPrefix (tokenStringProp2 @ExponentPrefix)

instance Arbitrary ExponentPrefix where
  arbitrary =
    ExponentPrefix <$> choose (True,False)

prop_Sign_TokenStringProp1 :: Sign -> Bool
prop_Sign_TokenStringProp1 = tokenStringProp1

prop_Sign_TokenStringProp2  =
  forAll arbSign (tokenStringProp2 @Sign)

instance Arbitrary Sign  where
  arbitrary = elements [PlusSign, MinusSign]


prop_DigitSequence_TokenStringProp1 :: DigitSequence -> Bool
prop_DigitSequence_TokenStringProp1 = tokenStringProp1

prop_DigitSequence_TokenStringProp2  =
  forAll arbDigitSequence (tokenStringProp2 @DigitSequence)

instance Arbitrary DigitSequence where
  arbitrary =
    DigitSequence <$> stringOf1 (elements ['0'..'9'])

prop_HexadecimalFractionalConstant_TokenStringProp1 :: HexadecimalFractionalConstant -> Bool
prop_HexadecimalFractionalConstant_TokenStringProp1 = tokenStringProp1

prop_HexadecimalFractionalConstant_TokenStringProp2  =
  forAll arbHexadecimalFractionalContant (tokenStringProp2 @HexadecimalFractionalConstant)

instance Arbitrary HexadecimalFractionalConstant where
  arbitrary = do
    oneof [ HexadecimalFractionalConstant <$> arbitrary <*> (Just <$> dseq)
          , HexadecimalFractionalConstant <$> (Just <$> dseq) <*> (pure Nothing)
          ]
      where
        dseq = arbitrary :: Gen HexadecimalDigitSequence

prop_BinaryExponentPart_TokenStringProp1 ::  BinaryExponentPart -> Bool
prop_BinaryExponentPart_TokenStringProp1 = tokenStringProp1

prop_BinaryExponentPart_TokenStringProp2  =
  forAll arbBinaryExponentPart (tokenStringProp2 @BinaryExponentPart)

instance Arbitrary BinaryExponentPart where
  arbitrary =
    BinaryExponentPart <$> arbitrary <*> arbitrary <*> arbitrary

prop_BinaryExponentPrefix_TokenStringProp1 ::  BinaryExponentPrefix -> Bool
prop_BinaryExponentPrefix_TokenStringProp1 = tokenStringProp1

prop_BinaryExponentPrefix_TokenStringProp2  =
  forAll arbBinaryExponenetPrefix (tokenStringProp2 @BinaryExponentPrefix)

instance Arbitrary BinaryExponentPrefix where
  arbitrary =
    BinaryExponentPrefix <$> choose (True, False)


prop_FloatSuffix_TokenStringProp1 :: FloatingSuffix -> Bool
prop_FloatSuffix_TokenStringProp1 = tokenStringProp1

prop_FloatSuffix_TokenStringProp2  =
  forAll arbFloatingSuffix (tokenStringProp2 @(Maybe FloatingSuffix))

instance Arbitrary FloatingSuffix where
  arbitrary = oneof [ Float_L <$> choose (True, False)
                    , Float_F <$> choose (True, False)
                    ]

return[]
allFloatingProperties = $allProperties
