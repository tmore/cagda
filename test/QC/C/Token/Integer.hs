-- | Author: Tomas Möre
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TemplateHaskell #-}
module QC.C.Token.Integer where

import QC.C.Token.TokenString
import QC.C.Token.TokenGen

import C.Token.Integer

import Test.QuickCheck
import Control.Monad

import qualified Data.Char as Char


prop_IntegerConst_TokenStringProp1 :: IntegerConstant -> Bool
prop_IntegerConst_TokenStringProp1 a =
  tokenStringProp1 a

--prop_intConstTokenStringProp2 :: IntegerString -> Bool
prop_IntegerConst_TokenStringProp2  =
  forAll arbIntegerConstant (tokenStringProp2 @IntegerConstant)


instance Arbitrary IntegerConstant where
  arbitrary =
    join $ elements [ DecIC <$> arbitrary <*> arbitrary
                    , OctIC <$> arbitrary <*> arbitrary
                    , HexIC <$> arbitrary <*> arbitrary
                    ]


prop_HexadecimalConstant_TokenStringProp1 :: HexadecimalConstant -> Bool
prop_HexadecimalConstant_TokenStringProp1 = tokenStringProp1

--prop_hexTokenStringProp2 :: HexString -> Bool
prop_HexadecimalConstant_TokenStringProp2  =
  forAll arbHexadecimalConstant (tokenStringProp2 @HexadecimalConstant)

instance Arbitrary HexadecimalConstant where
  arbitrary =
    HexC <$> arbitrary <*> arbitrary


prop_OctalConstant_TokenStringProp1 :: OctalConstant -> Bool
prop_OctalConstant_TokenStringProp1 = tokenStringProp1

--prop_octTokenStringProp2 :: OctString -> Bool
prop_OctalConstant_TokenStringProp2  =
  forAll arbOctalConstant (tokenStringProp2 @OctalConstant)

instance Arbitrary OctalConstant where
  arbitrary = do
    str <- arbOctalConstant
    pure $ OctC str

prop_DecimalConstant_TokenStringProp1 :: DecimalConstant -> Bool
prop_DecimalConstant_TokenStringProp1 = tokenStringProp1

--prop_decTokenStringProp2 :: DecimalString -> Bool
prop_DecimalConstant_TokenStringProp2  =
  forAll arbDecimalConstant (tokenStringProp2 @DecimalConstant)

instance Arbitrary DecimalConstant where
  arbitrary =
    DecC <$> arbDecimalConstant

prop_HexadecimalPrefix_PrefixTokenStringProp1 :: HexadecimalPrefix -> Bool
prop_HexadecimalPrefix_PrefixTokenStringProp1 = tokenStringProp1

--prop_integerSufixTokenStringProp2 :: IntegerSuffixString -> Bool
prop_HexadecimalPrefix_PrefixTokenStringProp2  =
  forAll arbHexadecimalPrefix (tokenStringProp2 @HexadecimalPrefix)

instance Arbitrary HexadecimalPrefix where
  arbitrary = elements [Hex0x, Hex0X]

prop_HexadecimalDigitSequence_TokenStringProp1 :: HexadecimalDigitSequence -> Bool
prop_HexadecimalDigitSequence_TokenStringProp1 = tokenStringProp1

--prop_integerSufixTokenStringProp2 :: IntegerSuffixString -> Bool
prop_HexadecimalDigitSequence_TokenStringProp2  =
  forAll arbHexadecimalDigitSequence (tokenStringProp2 @HexadecimalDigitSequence)

instance Arbitrary HexadecimalDigitSequence where
  arbitrary = HexDSeq <$> arbHexadecimalDigitSequence


prop_IntegerSuffix_TokenStringProp1 :: IntegerSuffix -> Bool
prop_IntegerSuffix_TokenStringProp1 = tokenStringProp1

--prop_integerSufixTokenStringProp2 :: IntegerSuffixString -> Bool
prop_IntegerSuffix_TokenStringProp2  =
  forAll arbIntegerSuffix (tokenStringProp2 @(Maybe IntegerSuffix))

instance Arbitrary IntegerSuffix where
  arbitrary =
    oneof [ Int_U <$> u
          , Int_UL <$> (u <<>> l)
          , Int_ULL <$> (u <<>> ll)
          , Int_L <$> l
          , Int_LU <$> (l <<>> u)
          , Int_LL <$> ll
          , Int_LLU <$> (ll <<>> u)
          ]
    where
      u :: Gen String
      u = elements ["u", "U"]

      l = elements ["l", "L"]
      ll = elements ["ll", "LL"]

return[]
allIntegerProperties = $allProperties
