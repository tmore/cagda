import Test.HUnit

import qualified QC.Tests as QC
import qualified HUnit.Spec as HUnit

main :: IO ()
main = do
  putStrLn "Running Quickcheck tests "
  QC.runTests

  putStrLn "Running unit tests"
  HUnit.runTests
  pure ()
