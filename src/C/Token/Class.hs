-- | Author: Tomas Möre
--
-- This module provides a simple inteferace for all tokens that are isomophic to
-- some string
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module C.Token.Class where

import Text.Megaparsec

import Data.String

import C.Token.Util

-- | Class of tokens that can are semantically isomorphic to strings.
--
-- The rules are that:
-- forall a   . parseString (toString (normalize a)) == Just (normalize a)
-- forall tokenstr . toString (parseString tokenstr) == tokenstr
class TokenString a where
  toString :: (Monoid s, IsString s, Eq s) => a -> s

  parser :: MonadTokenParsec s m => m a
  {-# Minimal (toString , parser) #-}

  parseString :: String -> Maybe a
  parseString =
    parseMaybe parser

  -- | Some TokenStrings may be represented in such a way that that the first
  -- first propery doesn't hold without normalisation. For example a string with
  -- a hexadecimal escape squence followed by any hexadecimal character.
  normalize :: a -> a
  normalize = id

-- | In the current semantics optional values are either there or completely
-- missing. That is if it doesn't match Nothing is returned otherwise the value
-- is. When converted to a string the missing value is just the empty string
instance TokenString a => TokenString (Maybe a) where
  toString Nothing = ""
  toString (Just a) = toString a
  parser = optional (try parser)

-- | TokenEq defined semantic equality of tokens, any extra information that
-- carries no semantic meaning should not be used in this judgement. Such as if
-- the integer suffixes are lower or smaller capitals. If hex string are
-- uppercase or lowercase. positive signs in front of exponents.
--
-- This class is made to make it easier to parse things.
class TokenEq a where
  tokenEq :: a -> a -> Bool

-- | Default instance for maybe in case inner is tokenEq
instance (TokenEq a) => TokenEq (Maybe a) where
  tokenEq Nothing Nothing = True
  tokenEq (Just a) (Just b) = tokenEq a b
  tokenEq _ _ = False
