-- | Author: Tomas Möre 2019
--
-- ISO/IEC 9899:201x - 6.4.4.4 Character constants
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveGeneric #-}
module C.Token.Character where

import Text.Megaparsec hiding (Token)

import C.Token.Integer (octalDigit)
import C.Token.Class

import C.Token.Util

import qualified Data.List as List

import qualified Data.Char as Char
import GHC.Generics

data CharacterConstant where
  CharConst :: (Maybe CharPrefix) -> CCharSequence -> CharacterConstant
  deriving (Show, Read, Eq)

instance TokenEq CharacterConstant where
  tokenEq (CharConst a b) (CharConst c d) =
    tokenEq a c && tokenEq b d

instance TokenString CharacterConstant where
  toString (CharConst mPrefix ccSeq) =
    toString mPrefix <> "'" <> toString ccSeq <> "'"
  parser = characterConstant

  normalize (CharConst mPrefix scSeq) =
    CharConst mPrefix (normalize scSeq)

characterConstant :: MonadTokenParsec s m => m CharacterConstant
characterConstant = do
  prefix <- optional (try characterPrefix)
  char '\''
  seq <- cCharSequence
  char '\''
  pure $ CharConst prefix seq

data CharPrefix where
  WCharCP :: CharPrefix
  Char16CP :: CharPrefix
  Char32CP :: CharPrefix
  deriving (Show,Read, Eq, Generic)

instance TokenEq CharPrefix where
  tokenEq = (==)

instance TokenString CharPrefix where
  toString WCharCP = "L"
  toString Char16CP = "u"
  toString Char32CP = "U"
  parser = characterPrefix

characterPrefix :: MonadTokenParsec s m => m CharPrefix
characterPrefix =
  choice [ try (char 'u' >> pure Char16CP)
         , try (char 'U' >> pure Char32CP)
         , char 'L' >> pure WCharCP
         ]

newtype CCharSequence = CCharSequence [CChar]
  deriving (Show, Read, Eq, Generic)

instance TokenEq CCharSequence where
  tokenEq = (==)

instance TokenString CCharSequence where
  toString (CCharSequence cChars) = mconcat $ map toString cChars
  parser = cCharSequence

  normalize (CCharSequence inner) = CCharSequence $ normalize' inner
    where
      normalize' [] = []
      normalize' [a] = [a]
      normalize' (h1@(EscapeCC (HexES (HexadecEscapeSequence chars))) : h2@(SimpleCC c) : rest)
        | Char.isHexDigit c =
          normalize' ((EscapeCC (HexES (HexadecEscapeSequence (chars ++ [c]))) : rest))
        | otherwise = h1 : h2 : normalize' rest
      normalize' (h1@(EscapeCC (OctES (OctalEscapeSequence chars))) : h2@(SimpleCC c) : rest)
        | length chars >= 3 = h1 : h2 : normalize' rest
        | Char.isOctDigit c =
            normalize' ((EscapeCC (OctES (OctalEscapeSequence (chars ++ [c]))) : rest))
        | otherwise = h1 : h2 : normalize' rest
      normalize' (h:t) = h : normalize' t

cCharSequence :: MonadTokenParsec s m => m CCharSequence
cCharSequence =
  CCharSequence <$> some (try cChar)

data CChar where
  SimpleCC :: Char -> CChar
  EscapeCC :: EscapeSequence -> CChar
  deriving (Show, Read, Eq, Generic)

instance TokenEq CChar where
  tokenEq (SimpleCC a) (SimpleCC b) =
    a == b
  tokenEq (EscapeCC a) (EscapeCC b) =
    tokenEq a b

instance TokenString CChar where
  toString (SimpleCC c) = fromString [c]
  toString (EscapeCC s) = toString s

  parser = cChar

cChar :: MonadTokenParsec s m => m CChar
cChar = try (EscapeCC <$> escapeSeq)
    <|> simpleCC
  where
    simpleCC = do
      c <- anySingle
      if List.elem c disallowedChars
        then fail $ "Character may not be any of " <> show disallowedChars
        else pure (SimpleCC c)

    disallowedChars = ['\\', '\'', '\n', '\r']

data EscapeSequence where
  SimpleES :: SimpleEscapeSequence -> EscapeSequence
  OctES :: OctalEscapeSequence -> EscapeSequence
  HexES :: HexadecEscapeSequence -> EscapeSequence
  UcnES :: UniversalCharacterName -> EscapeSequence
  deriving (Show, Read, Eq, Generic)

instance TokenEq EscapeSequence where
  tokenEq (SimpleES a) (SimpleES b) =
    a == b
  tokenEq (OctES a) (OctES b) =
    a == b
  tokenEq (HexES a) (HexES b) =
    tokenEq a b
  tokenEq (UcnES a) (UcnES b) =
    tokenEq a b
  tokenEq _ _ = False

escapeSeq :: MonadTokenParsec s m => m EscapeSequence
escapeSeq =
  choice [ SimpleES <$> try simpleEscapeSequence
         , OctES    <$> try octalEscapeSequence
         , HexES    <$> try hexadecimalEscapeSequence
         , UcnES    <$> universalCharacterName
         ]

instance TokenString EscapeSequence where
  toString (SimpleES s) = toString s
  toString (OctES s) = toString s
  toString (HexES s) = toString s
  toString (UcnES s) = toString s

  parser = escapeSeq

newtype SimpleEscapeSequence = SimpleEscapeSequence Char
  deriving (Show, Read, Eq, Generic)

instance TokenString SimpleEscapeSequence where
  toString (SimpleEscapeSequence c) = fromString ['\\' , c]
  parser = simpleEscapeSequence

simpleEscapeSequence :: MonadTokenParsec s m => m SimpleEscapeSequence
simpleEscapeSequence = do
  char '\\'
  SimpleEscapeSequence <$> oneOf ("\'\"?\\abfnrtv" :: [Char])

newtype OctalEscapeSequence = OctalEscapeSequence String
  deriving (Show, Read, Eq)

instance TokenString OctalEscapeSequence where
  toString (OctalEscapeSequence a) = fromString $ '\\' : a
  parser = octalEscapeSequence

octalEscapeSequence :: MonadTokenParsec s m => m OctalEscapeSequence
octalEscapeSequence =
  char '\\' >> OctalEscapeSequence <$> validSeq
  where
    validSeq = try (octalDigits 3)
           <|> try (octalDigits 2)
           <|> octalDigits 1


    octalDigits n = sequenceA (take n $ repeat octalDigit)

newtype HexadecEscapeSequence = HexadecEscapeSequence String
  deriving (Show, Read, Eq, Generic)

instance TokenEq HexadecEscapeSequence where
  tokenEq (HexadecEscapeSequence a) (HexadecEscapeSequence b) =
    map Char.toLower a == map Char.toLower b

instance TokenString HexadecEscapeSequence where
  toString (HexadecEscapeSequence s) = fromString $ "\\x" <> s
  parser = hexadecimalEscapeSequence

hexadecimalEscapeSequence :: MonadTokenParsec s m => m HexadecEscapeSequence
hexadecimalEscapeSequence = do
  string "\\x"
  HexadecEscapeSequence <$> some (try hexDigitChar)

-- ISO/IEC 9899:201x 6.4.3 Universal character names

-- | The ISO standar specifies that an UniversalCharacterName is either one or
-- two quads, here, if the only is one quad in the input the first quad is zero
-- to '0000'
data UniversalCharacterName where
  UCN1 :: HexQuad -> UniversalCharacterName
  UCN2 :: HexQuad -> HexQuad -> UniversalCharacterName
  deriving (Show, Read, Eq, Ord)

instance TokenEq UniversalCharacterName where
  tokenEq (UCN1 a) (UCN1 b) =
    tokenEq a b
  tokenEq (UCN2 a b) (UCN2 c d) =
    tokenEq a c && tokenEq b d
  tokenEq _ _ = False

instance TokenString UniversalCharacterName where
  toString (UCN1 q) = "\\u" <> toString q
  toString (UCN2 q1 q2) = "\\U" <> toString q1 <> toString q2
  parser = universalCharacterName

data HexQuad = HexQuad Char Char Char Char
  deriving (Show, Read, Ord)

lowerCaseQuad :: HexQuad -> HexQuad
lowerCaseQuad (HexQuad a1 a2 a3 a4) =
  (HexQuad (Char.toLower a1) (Char.toLower a2)
           (Char.toLower a3) (Char.toLower a4))
instance Eq HexQuad where
  (==) (HexQuad a1 a2 a3 a4) (HexQuad b1 b2 b3 b4) =
    Char.toLower a1 == Char.toLower b1
    && Char.toLower a2 == Char.toLower b2
    && Char.toLower a3 == Char.toLower b3
    && Char.toLower a4 == Char.toLower b4


instance TokenEq HexQuad where
  tokenEq = (==)

instance TokenString HexQuad where
  toString = fromString . hexQuadToString
  parser = hexQuad

-- | ISO/IEC 9899:201x : 6.4.3 Universal character names
universalCharacterName :: MonadTokenParsec s m => m UniversalCharacterName
universalCharacterName = try singeQuad <|> dualQuad
  where
    dualQuad = do
      string "\\U"
      q1 <- hexQuad
      q2 <- if q1 == zeroQuad
            then verifyQuad =<< hexQuad
            else hexQuad
      pure $ UCN2 q1 q2
    singeQuad = do
      string "\\u"
      UCN1 <$> (verifyQuad =<< hexQuad)
    verifyQuad quad
      | any (quad==) [quad0024, quad0040, quad0060] =
        pure quad
      | quad < quad00A0 =
        fail "Universal character name may not be less then 00A0"
      | quadD800 <= quad && quad <= quadDFFF =
        fail "Universal character name may not be in the rnage D800-DFFF inclusive"
      | otherwise =
        pure quad

    quad0024 = unsafeFromString "0024"
    quad0040 = unsafeFromString "0040"
    quad0060 = unsafeFromString "0060"
    quad00A0 = unsafeFromString "00A0"
    quadD800 = unsafeFromString "D800"
    quadDFFF = unsafeFromString "DFFF"

    unsafeFromString [c1,c2,c3,c4] =
      (HexQuad c1 c2 c3 c4)

zeroQuad = HexQuad '0' '0' '0' '0'

hexQuadToString :: HexQuad -> String
hexQuadToString (HexQuad c1 c2 c3 c4) = [c1, c2, c3, c4]

hexQuad :: MonadTokenParsec s m => m HexQuad
hexQuad = HexQuad <$> hexaDecmalDigit
                  <*> hexaDecmalDigit
                  <*> hexaDecmalDigit
                  <*> hexaDecmalDigit

hexaDecmalDigit :: MonadTokenParsec s m => m Char
hexaDecmalDigit = alphaNumChar
