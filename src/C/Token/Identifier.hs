-- | Author: Tomas Möre 2019
--
-- ISO/IEC 9899:201x - 6.4.2 Identifiers
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
module C.Token.Identifier where

import Text.Megaparsec


import C.Token.Class
import C.Token.Character (universalCharacterName)
import Data.String
import Data.List

import C.Token.Util

newtype Identifier = Identifier String
  deriving (Show, Read, Eq)

instance TokenEq Identifier where
  tokenEq = (==)

instance TokenString Identifier where
  toString (Identifier s) = fromString s
  parser = identifier

identifier :: MonadTokenParsec s m => m Identifier
identifier = do
  start <- identifierNondigit
  rest <- many ( try identifierChar
               <|> try (fmap toString universalCharacterName))
  pure $ Identifier (start <> mconcat rest)

identifierChar :: MonadTokenParsec s m => m String
identifierChar = try identifierNondigit <|> (fmap pure digitChar)

identifierNondigit :: MonadTokenParsec s m => m String
identifierNondigit = try (nondigit)
                 <|> try (toString <$> universalCharacterName)


nondigit :: MonadTokenParsec s m => m String
nondigit = fmap pure $ oneOf $ sourceCharacterSet \\ ['0' .. '9']

-- digit is defined in Text.Parsec.Char
