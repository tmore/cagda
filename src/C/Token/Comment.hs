-- | Author: Tomas Möre 2019
--
-- Comments are treated slighly diffrent then from normal tokens, as entire
-- tokiens in fact. This is because they may contain extra information that will
-- be needed further down the road.
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
module C.Token.Comment where

import Text.Megaparsec hiding (Token)

import C.Token.Class
import C.Token.Util

data Comment where
  Comment :: CommentType -> String -> Comment
  deriving (Show, Read, Eq)

instance TokenEq Comment where
  tokenEq = (==)

data CommentType where
  SingleRow :: CommentType
  MultiRow :: CommentType
  deriving (Show, Read, Eq)

instance TokenString Comment where
  toString (Comment SingleRow content) = fromString $ "\\" <> content
  toString (Comment MultiRow content) = fromString $ "/*" <> content <> "*/"

  parser = comment

comment :: MonadTokenParsec s m => m Comment
comment = try ((Comment SingleRow) <$> singleRow)
       <|> ((Comment MultiRow) <$> multiRow)

singleRow :: MonadTokenParsec s m => m String
singleRow = do
  string "//"
  commentContent <- manyTill anySingle (try (char '\n'))
  pure commentContent

multiRow :: MonadTokenParsec s m => m String
multiRow = do
  string "/*"
  content <- manyTill anySingle (try (string "*/"))
  pure $ content
