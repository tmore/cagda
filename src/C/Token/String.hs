-- | Author: Tomas Möre 2019
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module C.Token.String where

import Text.Megaparsec

import C.Token.Util
import C.Utils
import C.Token.Character

import C.Token.Class

import GHC.Generics

import qualified Data.List as List
import qualified Data.Char as Char

data StringLiteral where
  StringLit :: Maybe EncodingPrefix -> SCharSequence -> StringLiteral
  deriving (Show, Read, Eq, Generic)

instance TokenEq StringLiteral where
  tokenEq (StringLit a b) (StringLit c d) =
    tokenEq a c && tokenEq b d

instance TokenString StringLiteral where
  toString (StringLit prefix charseq) =
    toString prefix <> "\"" <> toString charseq <> "\""
  parser = stringLiteral

  normalize (StringLit encPrefix sseq) =
    StringLit encPrefix (normalize sseq)

stringLiteral :: MonadTokenParsec s m => m StringLiteral
stringLiteral =
  StringLit <$> optional (try encodingPrefix)
            <*> (char '"' *> sCharSequence <* char '"')

data SChar where
  SimpleSC :: Char -> SChar
  EscapeSC :: EscapeSequence -> SChar
  deriving (Show, Read, Eq, Generic)

instance TokenEq SChar where
  tokenEq (SimpleSC a) (SimpleSC b) =
    a == b
  tokenEq (EscapeSC a) (EscapeSC b) =
    tokenEq a b
  tokenEq _ _ = False

instance TokenString SChar where
  toString (SimpleSC c) = charToString c
  toString (EscapeSC s) = toString s
  parser = sChar

sChar :: MonadTokenParsec s m => m SChar
sChar = try (EscapeSC <$> escapeSeq)
    <|> simpleCC
  where
    simpleCC = do
      c <- anySingle
      if List.elem c disallowedChars
        then fail $ "Character may not be any of " <> show disallowedChars
        else pure (SimpleSC c)
    disallowedChars = ['\\', '\"', '\n', '\r']

newtype SCharSequence = SCharSequence [SChar]
  deriving (Show, Read, Eq, Generic)

instance TokenEq SCharSequence where
  tokenEq (SCharSequence a) (SCharSequence b) =
    and (zipWith tokenEq a b)

instance TokenString SCharSequence where
  toString (SCharSequence s) = mconcat $ fmap toString s
  parser = sCharSequence

  normalize (SCharSequence inner) = SCharSequence $ normalize' inner
    where
      normalize' [] = []
      normalize' [a] = [a]
      normalize' (h1@(EscapeSC (HexES (HexadecEscapeSequence chars))) : h2@(SimpleSC c) : rest)
        | Char.isHexDigit c =
          normalize' ((EscapeSC (HexES (HexadecEscapeSequence (chars ++ [c]))) : rest))
        | otherwise = h1 : h2 : normalize' rest
      normalize' (h1@(EscapeSC (OctES (OctalEscapeSequence chars))) : h2@(SimpleSC c) : rest)
        | length chars >= 3 = h1 : h2 : normalize' rest
        | Char.isOctDigit c =
            normalize' ((EscapeSC (OctES (OctalEscapeSequence (chars ++ [c]))) : rest))
        | otherwise = h1 : h2 : normalize' rest
      normalize' (h:t) = h : normalize' t

sCharSequence :: MonadTokenParsec s m => m SCharSequence
sCharSequence =
  SCharSequence <$> many (try sChar)

data EncodingPrefix where
  Enc_u8 :: EncodingPrefix
  Enc_u :: EncodingPrefix
  Enc_U :: EncodingPrefix
  Enc_L :: EncodingPrefix
  deriving (Show, Read, Eq)

instance TokenEq EncodingPrefix where
  tokenEq = (==)

instance TokenString EncodingPrefix where
  toString Enc_u8 = "u8"
  toString Enc_u = "u"
  toString Enc_U = "U"
  toString Enc_L = "L"

  parser = encodingPrefix

encodingPrefix :: MonadTokenParsec s m => m EncodingPrefix
encodingPrefix = choice [ try $ const Enc_u8 <$> string "u8"
                       , try $ const Enc_u <$> char 'u'
                       , try $ const Enc_U <$> char 'U'
                       , const Enc_L <$> char 'L'
                       ]
