-- | Author: Tomas Möre 2019
--
-- This module defines some utility token functions that does not fit anywhere
-- else (Such as things found in the appendix)
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}

module C.Token.Util
  ( module Text.Megaparsec.Char
  , module Data.String
  , module ParserUtils
  , MonadTokenParsec
  , TokenError(..)
  , multiCharOps
  , singleCharOps
  , sourceCharacterSet
  ) where

import Text.Megaparsec hiding (Token)
import qualified Text.Megaparsec as MP

import ParserUtils
import Data.String
import Text.Megaparsec.Char

type MonadTokenParsec s m = (MonadParsec TokenError s m, Stream s, MP.Token s ~ Char, IsString (Tokens s), Eq s)

data TokenError where
  NoError :: TokenError
  deriving (Show, Read, Eq, Ord)

multiCharOps :: [String]
multiCharOps = [ "->", "+=", "-=", "*=", "/=", "%=", "^=", "&=", "|=", "||", "&&", "++", "--", "==", "<<=", ">>=", "<=", ">=", "<<", ">>"]


singleCharOps :: [Char]
singleCharOps = "{}[]()~!.,=-+*^/%><?:;"


--  5.2.1 Character sets
sourceCharacterSet :: (IsString s, Monoid s) => s
sourceCharacterSet = fromString $
  mconcat [ ['a' .. 'z']
          , ['A' .. 'Z']
          , ['0'..'9']
          , "!\"#%&'()*+,-./:;<=>?[\\]^_{|}~"
          ]
