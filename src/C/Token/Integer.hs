-- | Author: Tomas Möre 2019
--
-- As defined in ISO/IEC 9899:201x 6.4.4.1
--
-- The structure is slightly modifies to remove all implicit information

{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module C.Token.Integer where

import Text.Megaparsec hiding (Token)

import Control.Monad

import qualified Data.Char as Char
import Data.String

import C.Token.Class
import C.Token.Util

import GHC.Generics

data IntegerConstant where
  DecIC :: DecimalConstant -> (Maybe IntegerSuffix) -> IntegerConstant
  OctIC :: OctalConstant -> (Maybe IntegerSuffix) -> IntegerConstant
  HexIC :: HexadecimalConstant -> (Maybe IntegerSuffix) -> IntegerConstant
  deriving (Show, Read, Eq, Generic)

integerConstOptSuffix :: IntegerConstant -> Maybe IntegerSuffix
integerConstOptSuffix (DecIC _ mIS) = mIS
integerConstOptSuffix (OctIC _ mIS) = mIS
integerConstOptSuffix (HexIC _ mIS) = mIS

instance TokenEq IntegerConstant where
  tokenEq (DecIC a (Just b)) (DecIC c (Just d)) =
    tokenEq b d && tokenEq a c
  tokenEq (OctIC a (Just b)) (OctIC c (Just d)) =
    tokenEq b d && tokenEq a c
  tokenEq (HexIC a (Just b)) (HexIC c (Just d)) =
    tokenEq b d && tokenEq a c

instance TokenString IntegerConstant where
  toString val =
    let valStr = case val of
          DecIC d _ -> toString d
          OctIC d _ -> toString d
          HexIC d _ -> toString d
    in valStr <> toString (integerConstOptSuffix val)
  parser = integerConstant

integerConstant :: MonadTokenParsec s m => m IntegerConstant
integerConstant =
  choice [ try $ HexIC <$> hexadecimalConstant <*> optional (try integerSuffix)
         , try $ OctIC <$> octalConstant <*> optional (try integerSuffix)
         , DecIC <$> decimalConstant <*> optional (try integerSuffix)
         ]

newtype DecimalConstant where
  DecC :: String -> DecimalConstant
  deriving (Show, Read, Eq, Generic)

instance TokenEq DecimalConstant where
  tokenEq (DecC a) (DecC b) =
    map Char.toLower a == map Char.toLower b

instance TokenString DecimalConstant where
  toString (DecC s) = fromString s
  parser = decimalConstant

decimalConstant :: MonadTokenParsec s m => m DecimalConstant
decimalConstant =
  DecC <$> (try zero <|> nonZero)
  where
    zero = do
      char '0'
      lookAhead (try (void space) <|> try eof)
      pure "0"
    nonZero = do
      first <- nonZeroDigit
      mappend [first] <$> many (try digitChar)

newtype OctalConstant where
  OctC :: String -> OctalConstant
  deriving (Show, Read, Eq, Generic)

instance TokenEq OctalConstant where
  tokenEq (OctC a) (OctC b) =
    map Char.toLower a == map Char.toLower b

instance TokenString OctalConstant  where
  toString (OctC s) = fromString ('0':s)
  parser = octalConstant

octalConstant :: MonadTokenParsec s m => m OctalConstant
octalConstant =
  char '0' >> OctC <$> many (try octalDigit)

data HexadecimalConstant where
  HexC :: HexadecimalPrefix -> HexadecimalDigitSequence -> HexadecimalConstant
  deriving (Show, Read, Eq, Generic)

instance TokenEq HexadecimalConstant where
  tokenEq (HexC _ a) (HexC _ b) =
    tokenEq a b

instance TokenString HexadecimalConstant where
  toString (HexC prefix s) =
    toString prefix <> toString s
  parser = hexadecimalConstant

hexadecimalConstant :: MonadTokenParsec s m => m HexadecimalConstant
hexadecimalConstant = do
  HexC <$> hexadecimalPrefix <*> hexadecimalDigitSequence

data HexadecimalPrefix where
  Hex0x :: HexadecimalPrefix
  Hex0X :: HexadecimalPrefix
  deriving (Show, Read, Eq)

instance TokenString HexadecimalPrefix where
  toString (Hex0X) = "0X"
  toString (Hex0x) = "0x"
  parser = hexadecimalPrefix

hexadecimalPrefix :: MonadTokenParsec s m => m HexadecimalPrefix
hexadecimalPrefix = do
  char '0'
  x <- oneOf ['x', 'X']
  pure $ if x == 'x'
         then Hex0x
         else Hex0X

newtype HexadecimalDigitSequence = HexDSeq String
  deriving (Show, Read, Eq, Generic)

instance TokenEq HexadecimalDigitSequence where
  tokenEq (HexDSeq a) (HexDSeq b) =
    map Char.toLower a == map Char.toLower b

instance TokenString HexadecimalDigitSequence where
  toString (HexDSeq s) = fromString s
  parser = hexadecimalDigitSequence


hexadecimalDigitSequence :: MonadTokenParsec s m => m HexadecimalDigitSequence
hexadecimalDigitSequence =
  HexDSeq <$> some (try hexadecimalDigit)

-- | This differs slightly from the defenition in the book in the sense that it
-- inly parses the prefixes that are syntacially valid.
data IntegerSuffix where
  Int_U :: String -> IntegerSuffix
  Int_UL :: String -> IntegerSuffix
  Int_ULL :: String -> IntegerSuffix
  Int_L :: String -> IntegerSuffix
  Int_LU :: String -> IntegerSuffix
  Int_LL :: String -> IntegerSuffix
  Int_LLU :: String -> IntegerSuffix
  deriving (Show, Read, Eq, Generic)

instance TokenEq IntegerSuffix where
  tokenEq = (==)

instance TokenString IntegerSuffix where
  toString (Int_U s)   = fromString s
  toString (Int_UL s)  = fromString s
  toString (Int_ULL s) = fromString s
  toString (Int_L s)   = fromString s
  toString (Int_LU s)  = fromString s
  toString (Int_LL s)  = fromString s
  toString (Int_LLU s) = fromString s

  parser = integerSuffix

-- | Parses an integer suffix, If there are no suffix Nothing is returned
integerSuffix :: MonadTokenParsec s m => m IntegerSuffix
integerSuffix = do
  suffix <- some (try $ oneOf ("ULLull" :: [Char]))
  when (null suffix) $ fail "suffix string may not be empty"
  case map Char.toLower suffix of
    "u"   -> pure $ Int_U suffix
    "ul"  -> pure $ Int_UL suffix
    "ull" -> pure $ Int_ULL suffix
    "l"   -> pure $ Int_L suffix
    "lu"   -> pure $ Int_LU suffix
    "ll"  -> pure $ Int_LL suffix
    "llu" -> pure $ Int_LLU suffix
    _ -> fail "Invalid integer suffix sequence"

hexadecimalDigit :: MonadTokenParsec s m => m Char
hexadecimalDigit = satisfy Char.isHexDigit

nonZeroDigit :: MonadTokenParsec s m => m Char
nonZeroDigit = oneOf ['1' .. '9']

octalDigit :: MonadTokenParsec s m => m Char
octalDigit =  oneOf ['0' .. '7']
