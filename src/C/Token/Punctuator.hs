-- | Author: Tomas Möre 2019
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module C.Token.Punctuator where

import C.Token.Util
import Text.Megaparsec

import C.Token.Class

-- | ISO/IEC 9899:201x - 6.4.6 Punctuators
--
-- However the <: :> <% %> %: %:%: symbols are removed as puncuator as they are
-- semantically equivalent to other symbols.
data Punctuator where
  -- | '['
  LeftBracketP :: String -> Punctuator
  -- | ']'
  RightBracketP :: String -> Punctuator
  -- | '('
  LeftParanP :: Punctuator
  -- | ')'
  RightParanP :: Punctuator
  -- | '{'
  LeftCurlyBracketP :: String -> Punctuator
  -- | '}'
  RightCurlyBracketP :: String -> Punctuator
  -- | '.'
  DotP :: Punctuator
  -- | '->'
  ArrowP :: Punctuator
  -- | '++'
  IncrementP :: Punctuator
  -- | '--'
  DecrementP :: Punctuator
  -- | '&' can be both reference and bit and
  AmpersandP :: Punctuator
  -- | '*' can be both multiply, dereference and pointer
  AsteriskP :: Punctuator
  -- | '+'
  PlusP :: Punctuator
  -- | '-'
  MinusP :: Punctuator
  -- | '~'
  TildeP :: Punctuator
  -- | '!'
  NotP :: Punctuator
  -- | '/'
  DivideP :: Punctuator
  -- | '%'
  RemainderP :: Punctuator
  -- | '<<'
  LeftShitftP :: Punctuator
  -- | '>>'
  RightShiftP :: Punctuator
  -- | '<'
  LessP :: Punctuator
  -- | '>'
  GreaterP :: Punctuator
  -- | '<='
  LeqP :: Punctuator
  -- | '>='
  GeqP :: Punctuator
  -- | '=='
  EqualP :: Punctuator
  -- | '!='
  NotEqualP :: Punctuator
  -- | '^'
  BitXorP :: Punctuator
  -- | '|'
  BitOrP :: Punctuator
  -- | '&&'
  AndP :: Punctuator
  -- | '||'
  OrP :: Punctuator
  -- | '?'
  QuestionMarkP :: Punctuator
  -- | ':'
  ColonP :: Punctuator
  -- | ';'
  SemiColonP :: Punctuator
  -- | '...'
  TrippleDotP :: Punctuator
  -- | '=' can be both assign and initializer
  AssignP :: Punctuator
  -- | '*='
  MultiplyAssignP :: Punctuator
  -- | '/='
  DivideAssignP :: Punctuator
  -- | '%='
  RemainderAssignP :: Punctuator
  -- | '+='
  PlusAssignP :: Punctuator
  -- | '-='
  MinusAssignP :: Punctuator
  -- | '<<='
  LeftShiftAssignP :: Punctuator
  -- | '>>='
  RightShiftAssignP :: Punctuator
  -- | '&='
  BitAndAssignP :: Punctuator
  -- | '^='
  BitXorAssignP :: Punctuator
  -- | '|='
  BitOrAssignP :: Punctuator
  -- | ','
  CommaP :: Punctuator
  -- | '#'
  HashtagP :: String -> Punctuator
  -- | '##'
  HashtagHashtagP :: String -> Punctuator
  deriving (Show,Read, Eq)


instance TokenEq Punctuator where
  tokenEq (LeftBracketP _) (LeftBracketP _) = True
  tokenEq (RightBracketP _) (RightBracketP _) = True
  tokenEq (LeftCurlyBracketP _) (LeftCurlyBracketP _) = True
  tokenEq (RightCurlyBracketP _) (RightCurlyBracketP _) = True
  tokenEq (HashtagP _) (HashtagP _) = True
  tokenEq (HashtagHashtagP _) (HashtagHashtagP _) = True
  tokenEq a b = a == b

instance TokenString Punctuator where
  toString = puncuatorString
  parser = punctuator

punctuator :: MonadTokenParsec s m => m Punctuator
punctuator = try multiCharOp <|> singeCharOp

multiCharOp :: MonadTokenParsec s m => m Punctuator
multiCharOp = choice $ map (try . toParser) multiCharPunctAssocList

singeCharOp :: MonadTokenParsec s m => m Punctuator
singeCharOp = choice $ map (try . toParser) singleCharPunctAssocList

toParser :: MonadTokenParsec s m => (Tokens s, Punctuator) ->  m Punctuator
toParser (str, punct) = string str >> pure punct

singleCharPunctAssocList :: IsString s => [(s, Punctuator)]
singleCharPunctAssocList =
  [ ("[", LeftBracketP "[")
  , ("]", RightBracketP "]")
  , ("(", LeftParanP)
  , (")", RightParanP)
  , ("{", LeftCurlyBracketP "{")
  , ("}", RightCurlyBracketP "}")
  , (".", DotP)
  , ("&", AmpersandP)
  , ("*", AsteriskP)
  , ("+", PlusP)
  , ("-", MinusP)
  , ("~", TildeP)
  , ("!", NotP)
  , ("/", DivideP)
  , ("%", RemainderP)
  , ("<", LessP)
  , (">", GreaterP)
  , ("^", BitXorP)
  , (")", BitOrP)
  , ("?", QuestionMarkP)
  , (":", ColonP)
  , (";", SemiColonP)
  , ("=", AssignP)
  , (",", CommaP)
  , ("#", HashtagP "#")
  ]

multiCharPunctAssocList :: IsString s => [(s, Punctuator)]
multiCharPunctAssocList =
  [ ("->", ArrowP)
  , ("++", IncrementP)
  , ("--", DecrementP)
  , ("<<", LeftShitftP)
  , (">>", RightShiftP)
  , ("<=", LeqP)
  , (">=", GeqP)
  , ("==", EqualP)
  , ("!=", NotEqualP)
  , ("&&", AndP)
  , ("||", OrP)
  , ("...", TrippleDotP)
  , ("*=", MultiplyAssignP)
  , ("/=", DivideAssignP)
  , ("%=", RemainderAssignP)
  , ("+=", PlusAssignP)
  , ("-=", MinusAssignP)
  , ("<<=", LeftShiftAssignP)
  , (">>=", RightShiftAssignP)
  , ("&=", BitAndAssignP)
  , ("^=", BitXorAssignP)
  , ("|=", BitOrAssignP)
  , ("##", HashtagHashtagP "##")
  , ("<:", LeftBracketP "<:")
  , (":>", RightBracketP ":>" )
  , ("<%", LeftCurlyBracketP "<%")
  , ("%>", RightCurlyBracketP "%>")
  , ("%:", HashtagP "%:")
  , ("%:%:", HashtagHashtagP "%:%:")
  ]

allPunctuators :: IsString s => [s]
allPunctuators =
  map fst multiCharPunctAssocList ++ map fst singleCharPunctAssocList

puncuatorString :: IsString s => Punctuator -> s
puncuatorString LeftParanP = "("
puncuatorString RightParanP = ")"
puncuatorString DotP = "."
puncuatorString AmpersandP = "&"
puncuatorString AsteriskP = "*"
puncuatorString PlusP = "+"
puncuatorString MinusP = "-"
puncuatorString TildeP = "~"
puncuatorString NotP = "!"
puncuatorString DivideP = "/"
puncuatorString RemainderP = "%"
puncuatorString LessP = "<"
puncuatorString GreaterP = ">"
puncuatorString BitXorP = "^"
puncuatorString BitOrP = "|"
puncuatorString QuestionMarkP = "?"
puncuatorString ColonP = ":"
puncuatorString SemiColonP = ";"
puncuatorString AssignP = "="
puncuatorString CommaP = ","
puncuatorString ArrowP = "->"
puncuatorString IncrementP = "++"
puncuatorString DecrementP = "--"
puncuatorString LeftShitftP = "<<"
puncuatorString RightShiftP = ">>"
puncuatorString LeqP = "<="
puncuatorString GeqP = ">="
puncuatorString EqualP = "=="
puncuatorString NotEqualP = "!="
puncuatorString AndP = "&&"
puncuatorString OrP = "||"
puncuatorString TrippleDotP = "..."
puncuatorString MultiplyAssignP = "*="
puncuatorString DivideAssignP = "/="
puncuatorString RemainderAssignP = "%="
puncuatorString PlusAssignP = "+="
puncuatorString MinusAssignP = "-="
puncuatorString LeftShiftAssignP = "<<="
puncuatorString RightShiftAssignP = ">>="
puncuatorString BitAndAssignP = "&="
puncuatorString BitXorAssignP = "^="
puncuatorString BitOrAssignP = "|="
puncuatorString (HashtagHashtagP s) = fromString s
puncuatorString (LeftBracketP s) = fromString s
puncuatorString (RightBracketP s) = fromString s
puncuatorString (LeftCurlyBracketP s) = fromString s
puncuatorString (RightCurlyBracketP s) = fromString s
puncuatorString (HashtagP s) = fromString s
