-- | Author: Tomas Möre 2019
--
-- As defined in ISO/IEC 9899:201x - 6.4.4.2 Floating constants
--
-- The structure is slightly modifies to remove all implicit information

{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module C.Token.Floating where

import Text.Megaparsec hiding (Token)

import C.Token.Class
import C.Token.Integer (HexadecimalPrefix(..)
                       , hexadecimalPrefix
                       , HexadecimalDigitSequence(..)
                       , hexadecimalDigitSequence)

import C.Token.Util
import C.Utils

import qualified Data.Char as Char
import Data.Maybe

import Data.String

data FloatingConstant where
  -- | Decimal floating constant
  DecFloatC :: DecimalFloatingConstant -> FloatingConstant
  -- | Hexadecimal floating constant
  HexFloatC :: HexadecimalFloatingConstant -> FloatingConstant
  deriving (Show, Read, Eq)

instance TokenEq FloatingConstant where
  tokenEq (DecFloatC a) (DecFloatC b) =
    tokenEq a b
  tokenEq (HexFloatC a) (HexFloatC b) =
    tokenEq a b

instance TokenString FloatingConstant where
  toString (DecFloatC a) = toString a
  toString (HexFloatC a) = toString a

  parser = floatingConstant

floatingConstant :: MonadTokenParsec s m => m FloatingConstant
floatingConstant =
  choice [ DecFloatC <$> try decimalFloatingConstant
         , HexFloatC <$> hexadecimalFloatingConstant
         ]

data DecimalFloatingConstant where
  -- | Fractiona decimal constant
  FracC :: FractionalConstant -> (Maybe ExponentPart) -> (Maybe FloatingSuffix) -> DecimalFloatingConstant
  -- | Digit seqquence
  DSeqC :: DigitSequence -> ExponentPart -> (Maybe FloatingSuffix) -> DecimalFloatingConstant
  deriving (Show, Read, Eq)

instance TokenEq DecimalFloatingConstant where
  tokenEq (FracC a b c) (FracC d e f) =
    tokenEq a d && tokenEq b e && tokenEq c f
  tokenEq (DSeqC a b c) (DSeqC d e f) =
    tokenEq a d && tokenEq b e && tokenEq c f

instance TokenString DecimalFloatingConstant where
  toString (FracC fractC mExp mSuffix) =
    toString fractC <> toString mExp <> toString mSuffix
  toString (DSeqC dSeq exp mSuffix) =
    toString dSeq <> toString exp <> toString mSuffix
  parser = decimalFloatingConstant

decimalFloatingConstant :: MonadTokenParsec s m => m DecimalFloatingConstant
decimalFloatingConstant =
  choice [ try $ FracC <$> fractionalConstant
                       <*> optional (try exponentPart)
                       <*> optional (try floatingSuffix)
         , DSeqC <$> digitSequence
                 <*> exponentPart
                 <*> optional (try floatingSuffix)
         ]

data HexadecimalFloatingConstant where
  -- | HexadecimalFloatingconstant
  HexFracC :: HexadecimalPrefix -> HexadecimalFractionalConstant -> BinaryExponentPart -> Maybe FloatingSuffix -> HexadecimalFloatingConstant
  -- | HexadecimalDigitSequence
  HexDSeqC :: HexadecimalPrefix -> HexadecimalDigitSequence -> BinaryExponentPart -> Maybe FloatingSuffix -> HexadecimalFloatingConstant
  deriving (Show, Read, Eq)

instance TokenEq HexadecimalFloatingConstant where
  tokenEq (HexFracC _ a b c) (HexFracC _ d e f) =
    tokenEq a d && tokenEq b e && tokenEq c f
  tokenEq (HexDSeqC _ a b c) (HexDSeqC _ d e f) =
    tokenEq a d && tokenEq b e && tokenEq c f

instance TokenString HexadecimalFloatingConstant where
  toString (HexFracC prefix s mBExp mFS) =
    toString prefix <> toString s <> toString mBExp <> toString mFS
  toString (HexDSeqC prefix s bExp mFS) =
    toString prefix <> toString s <> toString bExp <> toString mFS
  parser = hexadecimalFloatingConstant

hexadecimalFloatingConstant :: MonadTokenParsec s m => m HexadecimalFloatingConstant
hexadecimalFloatingConstant =
  choice [ try $ HexFracC <$> hexadecimalPrefix
                          <*> hexadecimalFractionalConstant
                          <*> binaryExponentPart
                          <*> optional (try floatingSuffix)
         , HexDSeqC <$> hexadecimalPrefix
                    <*> hexadecimalDigitSequence
                    <*> binaryExponentPart
                    <*> optional (try floatingSuffix)
         ]

-- | Slight variation of the definition, structually this allows for empty
-- fractional constant, however the parsers here doesn't
data FractionalConstant where
  FractionalConstant :: Maybe DigitSequence -> Maybe DigitSequence -> FractionalConstant
  deriving (Show, Read, Eq)

instance TokenEq FractionalConstant where
  tokenEq = (==)

instance TokenString FractionalConstant where
  toString (FractionalConstant s1 s2) =
    toString s1 <> "." <> toString s2
  parser = fractionalConstant

fractionalConstant :: MonadTokenParsec s m => m FractionalConstant
fractionalConstant = do
  first <- optional (try digitSequence)
  char '.'
  second <- optional (try digitSequence)
  if isNothing first && isNothing second
    then fail "Fractional constant must have one digit sequence set"
    else pure $ FractionalConstant first second

data ExponentPart where
  Exp :: ExponentPrefix -> Maybe Sign -> DigitSequence -> ExponentPart
  deriving (Show, Read, Eq)

instance TokenEq ExponentPart where
  tokenEq (Exp _ a b) (Exp _ c d) =
    signEq a c && tokenEq b d
    where
      -- | No sign is equal to plus
      signEq Nothing (Just PlusSign) = True
      signEq (Just PlusSign) Nothing = True
      signEq a b = a == b

instance TokenString ExponentPart where
  toString (Exp prefix sign dec) =
    toString prefix <> toString sign <> toString dec
  parser = exponentPart

-- | Parses the exponent part for a floating number
exponentPart :: MonadTokenParsec s m => m ExponentPart
exponentPart =
  Exp <$> exponentPrefix <*> optional (try sign) <*> digitSequence

-- | ExpPrefix contains true if the E char is capital otherwise false
newtype ExponentPrefix = ExponentPrefix Bool
  deriving (Show, Read, Eq)

instance TokenEq ExponentPrefix where
  tokenEq _ _ = True

instance TokenString ExponentPrefix where
  toString (ExponentPrefix True) = "E"
  toString (ExponentPrefix False) = "e"
  parser = exponentPrefix

exponentPrefix :: MonadTokenParsec s m => m ExponentPrefix
exponentPrefix = ExponentPrefix . (=='E') <$> oneOf ['e', 'E']

data Sign where
  PlusSign :: Sign
  MinusSign :: Sign
  deriving (Show, Read, Eq)

instance TokenEq Sign where
  tokenEq = (==)

instance TokenString Sign where
  toString PlusSign = "+"
  toString MinusSign = "-"
  parser = sign

-- | Parses a sign.
sign :: MonadTokenParsec s m => m Sign
sign = toSign <$> oneOf ("+-" :: [Char])
  where
    toSign '+' = PlusSign
    toSign '-' = MinusSign
    toSign _ = undefined

newtype DigitSequence where
  DigitSequence :: String -> DigitSequence
  deriving (Show, Read, Eq)

instance TokenEq DigitSequence where
  tokenEq = (==)

instance TokenString DigitSequence where
  toString (DigitSequence s) = fromString s
  parser = digitSequence

-- | Parses a decimal integer sequence [0..9]+
digitSequence :: MonadTokenParsec s m => m DigitSequence
digitSequence = DigitSequence <$> some (try digitChar)

-- | Slight variation of the spec, techinally this allows for a single dot,
-- however, the parsers wont parse such a thing.
data HexadecimalFractionalConstant =
  HexFractionalConstant (Maybe HexadecimalDigitSequence) (Maybe HexadecimalDigitSequence)
  deriving (Show, Read, Eq)

instance TokenEq HexadecimalFractionalConstant where
  tokenEq (HexFractionalConstant a b) (HexFractionalConstant c d) =
    tokenEq a c && tokenEq b d

instance TokenString HexadecimalFractionalConstant where
  toString (HexFractionalConstant s1 s2) =
    toString s1 <> "." <> toString s2
  parser = hexadecimalFractionalConstant

hexadecimalFractionalConstant :: MonadTokenParsec s m => m HexadecimalFractionalConstant
hexadecimalFractionalConstant = do
  s1 <- optional (try hexadecimalDigitSequence)
  char '.'
  s2 <- optional (try hexadecimalDigitSequence)
  if isNothing s1 && isNothing s2
    then fail "Both sides of the dot can not be empty"
    else pure $ HexFractionalConstant s1 s2

data BinaryExponentPart where
  BinExp :: BinaryExponentPrefix -> Maybe Sign ->
            DigitSequence -> BinaryExponentPart
  deriving (Show, Read, Eq)

instance TokenEq BinaryExponentPart where
  tokenEq (BinExp _ a b) (BinExp _ c d) =
    signEq a c && tokenEq b d
    where
      -- | No sign is equal to plus
      signEq Nothing (Just PlusSign) = True
      signEq (Just PlusSign) Nothing = True
      signEq a b = a == b

instance TokenString BinaryExponentPart where
  toString (BinExp prefix mSign dSeq) =
    toString prefix <> toString mSign <> toString dSeq

  parser = binaryExponentPart

binaryExponentPart :: MonadTokenParsec s m => m BinaryExponentPart
binaryExponentPart =
  BinExp <$> binaryExponentPrefix
         <*> optional (try sign)
         <*> digitSequence

-- | If the innter value is true then the binary expnent was a capital P
-- otherwise a lowercase p
newtype BinaryExponentPrefix = BinaryExponentPrefix Bool
  deriving (Show, Read, Eq)

instance TokenEq BinaryExponentPrefix where
  tokenEq _ _ = True

instance TokenString BinaryExponentPrefix where
  toString (BinaryExponentPrefix True) = "P"
  toString (BinaryExponentPrefix False) = "p"

  parser = binaryExponentPrefix

binaryExponentPrefix :: MonadTokenParsec s m => m BinaryExponentPrefix
binaryExponentPrefix =
  (BinaryExponentPrefix . (=='P')) <$> oneOf ("pP" :: [Char])


data FloatingSuffix = Float_F Bool | Float_L Bool
  deriving (Show, Read, Eq)

instance TokenEq FloatingSuffix where
  tokenEq = (==)

instance TokenString FloatingSuffix where
  toString (Float_F isCapital) = charToString $ condCapitalize isCapital 'f'
  toString (Float_L isCapital) = charToString $ condCapitalize isCapital 'l'

  parser = floatingSuffix

-- | Parses some floating point suffic returning the lower case variant
floatingSuffix :: MonadTokenParsec s m => m FloatingSuffix
floatingSuffix = do
  c <- oneOf ("lLfF" :: [Char])
  let constr = case Char.toLower c of
        'l' -> Float_L
        'f' -> Float_F
        _ -> error "This should never be reaced"
  pure (constr (Char.isUpper c))

-- | Utility function to describe any fractionalConstant defined by the sequence
-- 'p'.
genenralFractionalConstant :: MonadTokenParsec s m => m String -> m String
genenralFractionalConstant p = try optInit <|> noInit
  where
    optInit = do
      mStart <- optional (try p)
      char '.'
      end <- p
      pure $ fromMaybe "" mStart <> "." <> end
    noInit = do
      char '.' >> (mappend ".") <$> p

expFloat :: MonadTokenParsec s m => m String
expFloat = do
      start <- many (try digitChar)
      mDot <- optional (try $ char '.')
      let dot = maybe "" (const ".") mDot
      postDot <- many (try digitChar)
      expSymbol <- oneOf ("eE" :: [Char])
      mSign <- optional (try $ oneOf ("+-":: [Char]))
      let sign = maybe "" pure mSign
      exp <- some (try digitChar)
      pure (start <> "." <> postDot <> pure expSymbol <> sign <> exp)
