-- | Author: Tomas Möre 2019
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module C.Token.CPP where

import Text.Megaparsec hiding (Token)
import C.Token.Util

import C.Token.Class

-- | Currntly empty, fill out later
data PreprocessingToken where
  HeaderName :: PreprocessingToken
  Identifier :: PreprocessingToken
  PPNumber :: PreprocessingToken
  CharacterConstant :: PreprocessingToken
  StringLitteral :: PreprocessingToken
  Punctuator :: PreprocessingToken
  Whitespace :: PreprocessingToken
  deriving (Show, Read, Eq)

instance TokenEq PreprocessingToken where
  -- Should be replaced once i actually can parse this!
  tokenEq = (==)

instance TokenString PreprocessingToken where
  toString _ = ""
  parser = pure HeaderName

-- | CPP tokens are treated specially note that for later use the input file should be run through the cpp first. Note probably not fully complete
cppRow :: MonadTokenParsec s m =>  m String
cppRow = do
  char '#'
  content <- manyTill anySingle (try (char '\n'))
  pure $ "#" <> content
