-- | Author: Tomas Möre 2019
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
module C.Token.Constant where

import Text.Megaparsec

import C.Token.Util

import C.Token.Class

import C.Token.Integer
import C.Token.Floating
import C.Token.Character

-- | ISO/IEC 9899:201x - 6.4.4 Constants
--
-- This section ignoresed enumeration consants as in the tokenizer phase it is
-- impossible to distingush them with plain identifiers
data Constant where
  -- | Integer constant
  IntC :: IntegerConstant -> Constant
  -- | Floating constant
  FloatC :: FloatingConstant -> Constant
  -- | Character constants
  CharC :: CharacterConstant -> Constant
  deriving (Show, Read, Eq)

instance TokenEq Constant where
  tokenEq (IntC a) (IntC b) = tokenEq a b
  tokenEq (FloatC a) (FloatC b) = tokenEq a b
  tokenEq (CharC a) (CharC b) = tokenEq a b
  tokenEq _ _ = False

instance TokenString Constant where
  toString (IntC intC) = toString intC
  toString (FloatC floatC) = toString floatC
  toString (CharC charC) = toString charC

  parser = constant

  normalize a@(IntC _) = a
  normalize a@(FloatC _) = a
  normalize (CharC c) = CharC $ normalize c

constant :: MonadTokenParsec s m => m Constant
constant =
  choice [ FloatC <$> try floatingConstant
         , IntC <$> try integerConstant
         , CharC <$> characterConstant
         ]
