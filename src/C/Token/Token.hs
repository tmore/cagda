-- | Author: Tomas Möre 2019
--
-- This module contains the hig level definition of a token.
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
module C.Token.Token
  ( module C.Token.Class
  , module C.Token.Keyword
  , module C.Token.Constant
  , module C.Token.Integer
  , module C.Token.Floating
  , module C.Token.Character
  , module C.Token.String
  , module C.Token.Punctuator
  , module C.Token.CPP
  , module C.Token.Comment
  , Token(..)
  , IsToken
  , toToken
  , fromToken
  , tokenIsType
  , token
  , tokenContains
  , CToken(..)

  ) where

import Text.Megaparsec hiding (Token, token)

import C.Token.Util

import C.Token.Class

import C.Token.Keyword
import C.Token.Identifier
import C.Token.Constant
import C.Token.Integer
import C.Token.Floating
import C.Token.Character
import C.Token.String
import C.Token.Punctuator
import C.Token.CPP
import C.Token.Comment

-- | General class of tokens
data Token where
  -- | Comments are an self added extension as they are where things such as
  -- extentisons can be added
  CommentTok :: Comment -> Token
  -- | Normal C token
  CTok :: CToken -> Token
  -- | Preprocessor token
  PPTok :: PreprocessingToken -> Token
  deriving (Show, Read, Eq)

instance TokenEq Token where
  tokenEq (CommentTok a) (CommentTok b) =
    tokenEq a b
  tokenEq (CTok a) (CTok b) =
    tokenEq a b
  tokenEq (PPTok a) (PPTok b) =
    tokenEq a b
  tokenEq _ _ = False

instance TokenString Token where
  toString (CTok a) = toString a
  toString (PPTok a) = toString a
  toString (CommentTok a) = toString a

  parser = token

token :: MonadTokenParsec s m => m Token
token = do
  choice [ CommentTok <$> try parser
         , CTok <$> try parser
         , PPTok <$> try parser
         ]

data CToken where
  -- | Keywords
  KwCT    :: Keyword -> CToken
  -- | Identifiers
  IdCT    :: Identifier -> CToken
  -- | Contants (char, int, float)
  ConstCT :: Constant -> CToken
  -- | String litterals
  StrCT   :: StringLiteral -> CToken
  -- | Punctuation
  PunctCT :: Punctuator -> CToken
  deriving (Show, Read, Eq)

instance TokenString CToken where
  toString (KwCT a) = toString a
  toString (IdCT a) = toString a
  toString (ConstCT a) = toString a
  toString (StrCT a) = toString a
  toString (PunctCT a) = toString a

  parser = ctoken

instance TokenEq CToken where
  tokenEq (KwCT a) (KwCT b) = tokenEq a b
  tokenEq (IdCT a) (IdCT b) = tokenEq a b
  tokenEq (ConstCT a) (ConstCT b) = tokenEq a b
  tokenEq (StrCT a) (StrCT b) = tokenEq a b
  tokenEq (PunctCT a) (PunctCT b) = tokenEq a b
  tokenEq _ _ = False

ctoken :: MonadTokenParsec s m => m CToken
ctoken = do
  choice [ KwCT    <$> try parser
         , IdCT    <$> try parser
         , ConstCT <$> try parser
         , StrCT   <$> try parser
         , PunctCT <$> parser
         ]

-- | Checks where a given token contains another (Works both direction by
-- deeinition).
tokenContains :: (IsToken a, IsToken b, TokenEq a, TokenEq b) => a -> b -> Bool
tokenContains a b = toToken a `tokenEq` toToken b

class IsToken a where
  toToken :: a -> Token
  fromToken :: Token -> Maybe a

-- | Simple boolean check if a given token is a type (variable must be pased via
-- type application)
tokenIsType :: forall a . (IsToken a) => Token -> Bool
tokenIsType = maybe False (const True) . (fromToken :: Token -> Maybe a)

instance IsToken Token where
  toToken = id
  fromToken = Just

instance IsToken Comment where
  toToken = CommentTok
  fromToken (CommentTok a) = Just a
  fromToken _ = Nothing

instance IsToken Keyword where
  toToken = CTok . KwCT
  fromToken (CTok (KwCT a)) = Just a
  fromToken _ = Nothing

instance IsToken Identifier where
  toToken = CTok . IdCT
  fromToken (CTok (IdCT a)) = Just a
  fromToken _ = Nothing

instance IsToken CToken where
  toToken = CTok
  fromToken (CTok a) = Just a
  fromToken _ = Nothing

instance IsToken Constant where
  toToken = toToken . ConstCT
  fromToken (CTok (ConstCT a)) = Just a
  fromToken _ = Nothing

instance IsToken IntegerConstant where
  toToken = toToken . IntC
  fromToken (CTok (ConstCT (IntC x))) = Just x
  fromToken _ = Nothing

instance IsToken FloatingConstant where
  toToken = toToken . FloatC
  fromToken (CTok (ConstCT (FloatC x))) = Just x
  fromToken _ = Nothing

instance IsToken CharacterConstant where
  toToken = toToken . CharC
  fromToken (CTok (ConstCT (CharC x))) = Just x
  fromToken _ = Nothing

instance IsToken StringLiteral where
  toToken = toToken . StrCT
  fromToken (CTok (StrCT x)) = Just x
  fromToken _ = Nothing

instance IsToken Punctuator where
  toToken = toToken . PunctCT
  fromToken (CTok (PunctCT x)) = Just x
  fromToken _ = Nothing

instance IsToken PreprocessingToken where
  toToken = PPTok
  fromToken (PPTok a) = Just a
  fromToken _ = Nothing
