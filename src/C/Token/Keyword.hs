-- | Author: Tomas Möre 2019
--
-- As defined in ISO/IEC 9899:201x 6.4.1
--
-- The structure is slightly modifies to remove all implicit information

{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
module C.Token.Keyword where

import C.Token.Util
import Text.Megaparsec

import Data.Map (Map)
import qualified Data.Map as Map

import Data.Maybe

import C.Token.Class

-- | ISO/IEC 9899:201x - 6.4.1 Keywords
--
-- The ord instance carries no inherent meaning and is only meant for being able
-- to use this in datastrucutres where it is needed
data Keyword =
  AutoKW
  | BreakKW
  | CaseKW
  | CharKW
  | ConstKW
  | ContinueKW
  | DefaultKW
  | DoKW
  | DoubleKW
  | ElseKW
  | EnumKW
  | ExternKW
  | FloatKW
  | ForKW
  | GotoKW
  | IfKW
  | InlineKW
  | IntKW
  | LongKW
  | RegisterKW
  | RestrictKW
  | ReturnKW
  | ShortKW
  | SignedKW
  | SizeofKW
  | StaticKW
  | StructKW
  | SwitchKW
  | TypedefKW
  | UnionKW
  | UnsignedKW
  | VoidKW
  | VolatileKW
  | WhileKW
  | AlignAsKW
  | AlignOfKW
  | AtomicKW
  | BoolKW
  | ComplexKW
  | GenericKW
  | ImaginaryKW
  | NoreturnKW
  | StaticAssertKW
  | ThreadLocalKW
  deriving (Show, Read, Eq, Ord)

instance TokenEq Keyword where
  tokenEq = (==)

instance TokenString Keyword where
  toString a = fromJust $ Map.lookup a keyWordStringMap
  parser = keyword
  parseString = keywordFromString

keyword :: MonadTokenParsec s m => m Keyword
keyword = do
  str <- some (try (try (char '_') <|> letterChar))
  case keywordFromString str of
    Nothing -> fail "Invalid string, expected keyword"
    Just kw -> pure kw

-- | Rewrite to map lookup? (would require Ord instance or hash instance)
keywordFromString :: (Eq s,IsString s) => s -> Maybe Keyword
keywordFromString "alignof" = Just AlignOfKW
keywordFromString "auto" = Just AutoKW
keywordFromString "break" = Just BreakKW
keywordFromString "case" = Just CaseKW
keywordFromString "char" = Just CharKW
keywordFromString "const" = Just ConstKW
keywordFromString "continue" = Just ContinueKW
keywordFromString "default" = Just DefaultKW
keywordFromString "do" = Just DoKW
keywordFromString "double" = Just DoubleKW
keywordFromString "else" = Just ElseKW
keywordFromString "enum" = Just EnumKW
keywordFromString "extern" = Just ExternKW
keywordFromString "float" = Just FloatKW
keywordFromString "for" = Just ForKW
keywordFromString "goto" = Just GotoKW
keywordFromString "if" = Just IfKW
keywordFromString "inline" = Just InlineKW
keywordFromString "int" = Just IntKW
keywordFromString "long" = Just LongKW
keywordFromString "register" = Just RegisterKW
keywordFromString "restrict" = Just RestrictKW
keywordFromString "return" = Just ReturnKW
keywordFromString "short" = Just ShortKW
keywordFromString "signed" = Just SignedKW
keywordFromString "sizeof" = Just SizeofKW
keywordFromString "static" = Just StaticKW
keywordFromString "struct" = Just StructKW
keywordFromString "switch" = Just SwitchKW
keywordFromString "typedef" = Just TypedefKW
keywordFromString "union" = Just UnionKW
keywordFromString "unsigned" = Just UnsignedKW
keywordFromString "void" = Just VoidKW
keywordFromString "volatile" = Just VolatileKW
keywordFromString "while" = Just WhileKW
keywordFromString "_Alignas" = Just AlignAsKW
keywordFromString "_Atomic" = Just AtomicKW
keywordFromString "_Bool" = Just BoolKW
keywordFromString "_Complex" = Just ComplexKW
keywordFromString "_Generic" = Just GenericKW
keywordFromString "_Imaginary" = Just ImaginaryKW
keywordFromString "_Noreturn" = Just NoreturnKW
keywordFromString "_Static_assert" = Just StaticAssertKW
keywordFromString "_Thread_local"  = Just ThreadLocalKW
keywordFromString _  = Nothing

keywordMap :: (Ord str, IsString str) => Map.Map str Keyword
keywordMap =
  Map.fromList $ map (\ s -> (s, fromJust (keywordFromString s))) keywordList

keyWordStringMap :: (Eq str, IsString str) => Map Keyword str
keyWordStringMap =
  Map.fromList $ map (\ s -> (fromJust (keywordFromString s), s)) keywordList

keywordList :: (IsString str) => [str]
keywordList = [ "alignof"
              , "auto"
              , "break"
              , "case"
              , "char"
              , "const"
              , "continue"
              , "default"
              , "do"
              , "double"
              , "else"
              , "enum"
              , "extern"
              , "float"
              , "for"
              , "goto"
              , "if"
              , "inline"
              , "int"
              , "long"
              , "register"
              , "restrict"
              , "return"
              , "short"
              , "signed"
              , "sizeof"
              , "static"
              , "struct"
              , "switch"
              , "typedef"
              , "union"
              , "unsigned"
              , "void"
              , "volatile"
              , "while"
              , "_Alignas"
              , "_Atomic"
              , "_Bool"
              , "_Complex"
              , "_Generic"
              , "_Imaginary"
              , "_Noreturn"
              , "_Static_assert"
              , "_Thread_local"
              ]
