-- | Author: Tomas Möre 2019
--
-- ISO/IEC 9899:201x - 6.4.4.4 Character constants
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module C.Token.Enumeration where
