-- | Author: Tomas Möre 2019

{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}

module C.Token.Base where

import Control.Lens

import Data.String

import Text.Megaparsec hiding (Token)


data Token s = Token
                { _sourcePos :: SourcePos
                , _tokenValue  :: s
                }
makeLenses ''Token

instance (Eq s, IsString s, Semigroup s) => Semigroup (Token s) where
  (<>) a b = Token sp (_tokenValue a <> _tokenValue b)
    where
      sp = if isStaticToken a
           then _sourcePos b
           else _sourcePos a

instance (IsString s, Eq s, Monoid s) => Monoid (Token s) where
  mempty = Token nullSourcePos mempty

instance (Show s) => Show (Token s) where
  show = show . _tokenValue

instance (Read s) => Read (Token s) where
  readsPrec n = readsPrec n >>= pure . fmap (\ (s,r) -> (mkToken s, r))

instance (IsString s) => IsString (Token s) where
  fromString = mkToken . fromString

instance Eq s => Eq (Token s) where
  (==) a b = (a^.tokenValue) == (b^.tokenValue)
  (/=) a b = (a^.tokenValue) /= (b^.tokenValue)

instance Functor Token where
  fmap = over tokenValue

nullSourcePosName :: IsString s => s
nullSourcePosName = fromString "__STATIC VALUE__"

nullSourcePos :: SourcePos
nullSourcePos = SourcePos "" (mkPos 0) (mkPos 0)

isStaticToken :: (IsString s, Eq s) => Token s -> Bool
isStaticToken s = sourceName (_sourcePos s) == nullSourcePosName

-- | Makes a lexeme using the nullpos from the static source values
mkToken s =  Token nullSourcePos s

-- tokens :: Stream s Identity Char => Parsec s u [Token String]
-- tokens = manyTill (spaces >> oneToken) (try eof)

-- oneToken :: Stream s Identity Char => Parsec s u (Token String)
-- oneToken = do
--   pos <- getPosition
--   lex <- choice [ try comment
--                 , try cppRow
--                 , try stringLit
--                 , try charLit
--                 , try multiCharOp
--                 , try singleCharOp
--                 , try identifier
--                 , number
--                 ]
--   pure $ Lexeme pos lex
