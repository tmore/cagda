-- | Author: Tomas Möre 2019
--
-- This module defines everything a C type can be.

{-# LANGUAGE GADTs #-}
module C.Types where

import Data.Word
import Data.Text (Text)
import qualified Data.Text as T
import Data.Map (Map)
import Data.String

-- | General labelnames (
newtype LabelName = LabelName Text
  deriving (Show, Read, Eq, Ord)

class IsLabelName a where
  fromLabelName :: LabelName -> a

-- instance IsLabelName [Char] where
--   fromLabelName (LabelName s) = s

instance IsString LabelName where
  fromString = LabelName . fromString

-- | General variable names
newtype VarName = VarName Text
  deriving (Show, Read, Eq, Ord)

instance IsLabelName VarName where
  fromLabelName (LabelName s) = VarName s

instance IsString VarName where
  fromString = VarName . fromString

-- | General variable names
newtype FunctionName = FunctionName Text
  deriving (Show, Read, Eq, Ord)

instance IsLabelName FunctionName where
  fromLabelName (LabelName s) = FunctionName s

instance IsString FunctionName where
  fromString = FunctionName . fromString

-- | General type names
newtype TypeName = TypeName Text
  deriving (Show, Read, Eq, Ord)

instance IsLabelName TypeName where
  fromLabelName (LabelName s) = TypeName s
instance IsString TypeName where
  fromString = TypeName . fromString

-- | Serves as a way to uniquely identify a label. (The textual name is not
-- allways unique)
newtype LablelId = LabelId Word64
  deriving (Show, Read, Eq)
-- data CLabel = CLabel LabelName LabelId
-- | The primitive int types described by their byte sizes. It should be assumed
-- that the Signed versions are encoded by the two's complement
data PrimInt = SI Word16
             | UI Word16
             deriving (Show, Read, Eq)
newtype PrimFloat = PrimFloat Word16
  deriving (Show, Read, Eq)

data Primitive = IntP PrimInt
               | FloatP PrimFloat
               | UnitP
               | BooleanP
               deriving (Show, Read, Eq)
-- |
newtype StructType = StructType ([(Text, Type)])
  deriving (Show, Read, Eq)

-- | The type of static arrays
data ArrayType = ArrayType Word64 Type
  deriving (Show, Read, Eq)

-- | Abstract functon type (Only in and out types), This is becaise in the type
-- level we do not care about the names of the variables, only their order.
data FunctionType = FunctionType [Type] Type
  deriving (Show,Read, Eq)

-- | Defenition of the general C types
data Type where
  PrimT :: Primitive -> Type
  EnumT :: [(LabelName, Word64)] -> Type

  FunctionT :: FunctionType -> Type

  -- | Represents pointers to some type
  PtrT :: Type -> Type
  -- | Represents a constant variable, note that this introduces the problem of
  -- having recursive Const, which in fact doesn't matter as semantically forall
  -- a . Const (Const a) = to Const a
  ConstT :: Type -> Type
  -- | Represents all C arrays of known size (static)
  ArrayT :: ArrayType -> Type
  -- | Represents all structs in C First comes the name, secondly comes
  -- the internal representation
  StructT :: StructType -> Type
  deriving (Show, Read, Eq)
