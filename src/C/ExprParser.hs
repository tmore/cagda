{-|
Author: Tomas Möre 2019
-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module C.ExprParser where

-- import C.Expr
-- import C.Types
-- import C.Lexeme (Lexeme(..))
-- import C.Utils

-- import Control.Lens
-- import Control.Applicative hiding (Const, (<|>), many)

-- import Text.Parsec
-- import Text.Parsec.Combinator
-- import qualified Text.Parsec.Char as PC
-- import Text.Read (readMaybe)

-- import qualified Data.Text as T
-- import Data.Map
-- import Data.Either
-- import Data.Int
-- import Data.Word
-- import Data.Bits
-- import Data.List (find)
-- import Data.Scientific

-- import qualified Data.Char as Char
-- import qualified Data.Set as Set



-- data PrimeIntInfo =
--   PrimeIntInfo { _bitSize :: Word16
--                , _minValue :: Integer
--                , _maxValue :: Integer
--                } deriving (Show, Read, Eq)

-- intInfo :: forall n . (Bits n, Integer n, Bounded n) => PrimeIntInfo
-- intInfo = PrimeIntInfo (finiteBitSize (0 @n)) (minValue @n) (maxValue @n)

-- intInfoType :: PrimeIntInfo -> PrimInt
-- intInfoType PrimeInfo{..} =
--   if _minValue == 0
--   then UI _bitSize
--   else SI _bitSize

-- data PrimeFloatInfo =
--   PrimeFloatInfo { _bitSize :: Word16
--                  } deriving (Show, Read, Eq)

-- -- | Record to enble dynamic parsing and semantic meaning of primitive
-- -- types. This only covers the basic C primitives, exentions should be handles
-- -- separately. They are typed explicitly as to ensure they're all present in the
-- -- parsing. Further only the main categories are given
-- data PrimeInfo =
--   PrimeInfo { _pointer :: PrimteIntInfo
--             , _char  :: PrimeIntInfo
--             , _uChar :: PrimeIntInfo
--             , _sChar :: PrimeIntInfo
--             , _short :: PrimeIntInfoo
--             , _uShort :: PrimeIntInfo
--             , _int :: PrimeIntInfo
--             , _unsinged :: PrimeIntInfo
--             , _long :: PrimeIntInfo
--             , _uLong :: PrimeIntInfo
--             , _longLong :: PrimeIntInfoo
--             , _float :: PrimeFloatInfo
--             , _double :: PrimeFloatInfo
--             , _longDouble :: PrimeFloatInfo
--             } deriving (Show, Read, Eq)

-- basePrimInfo :: PrimeInfo
-- basePrimInfo =
--   PrimeInfo { _pointer = intInfo @Word64
--             , _char  = intInfo @Int8
--             , _uChar = intInfo @Word8
--             , _sChar = intInfo @Int8
--             , _short = intInfo @Int16
--             , _int = intInfo @Int32
--             , _unsinged = intInfo @Word32
--             , _long = intInfo @Int32
--             , _uLong = intInfo @Word32
--             , _longLong = intInfo @Int64
--             , _uLongLong = intInfo @Word64
--             , _float = PrimeFloatInfo 32
--             , _double = PrimeFloatInfo 64
--             , _longDouble = PrimeFloatInfo 128
--             }
-- data CContext =
--   CContext {
--   -- | Contains the information about the considered prime types, exists here as a guarantee that they exists
--   _primeInfo :: PrimeInfo
--   -- | Contains a mapping for type aliases basically all typdefs that arent
--   -- structs and the default aliases such as "short int" is an alias of "short"
--   , _typeAliases :: Map TypeName TypeName
--   -- | Contains all actual types, should not contain aliases directly
--   , _types :: Map TypeName Type
--   } deriving (Show, Read, Eq)

-- -- | Default simpe datatype map that comes with C by default (well not entierly
-- -- true because C doesn't come with any guarantees. This is a problem for future
-- -- me)
-- defaultCContext =
--   let intTypeOf getter = intInfoType $ getter basePrimInfo
--   in
--     CContext
--     { _primeInfo = basePrimInfo
--     , _typeAliases = fromList $ concat [ aliasify "short" ["short int", "short short int", "signed short"]
--                                        , aliasify "unsigned short" ["unsigned short int"]
--                                        , aliasify "int" ["signed", "signed int"]
--                                        , aliasify "unsigned" ["unsigned int"]
--                                        , aliasify "long" ["long int", "singed long", "signed long int"]
--                                        , aliasify "unsigned long" ["unsigned long int"]
--                                        , aliasify "long long" ["long long int", "signed long long", "signed long long int"]
--                                        , aliasify "unsigned long long" ["unsigned long long int"]
--                                        ]
--     , _types = fromList $ map (\(name, inner) -> (name, LabledType name inner))
--                        [ ("char", intTypeOf _char
--                        , ("unsigned char", intTypeOf _uChar)
--                        , ("signed char", intTypeOf _sChar)
--                        , ("short", intTypeOf _short)
--                        , ("unsigned short", intTypeOf _uShort)
--                        , ("int", intTypeOf _int)
--                        , ("long", intTypeOf _long)
--                        , ("unsigned long", intTypeOf _unsingedLong)
--                        , ("long long", intTypeOf _longLong)
--                        , ("unsigned long long", intTypeOf _uLongLong)


--                        , ("float", PrimT (FloatP (PrimFloat 32)))
--                        , ("double", PrimT (FloatP (PrimFloat 64)))
--                        , ("long double", PrimT (FloatP (PrimFloat 128)))
--                        ]

--   }

-- -- | Retrieves a  PrimIntInfo using a specific getter
-- getPrimeInt :: Stream s m Char => (PrimInfo -> PrimIntInfo) -> ParsecT s u m PrimIntInfo
-- getPrimeInt getter = getContext >>= pure . getter . _primInfo

-- -- | Retrieves a PrinFloatInfo using a specific getter
-- getPrimeFloat :: Stream s m Char => (PrimInfo -> PrimFloatInfo) -> ParsecT s u m PrimFloatInfo
-- getPrimeFloat getter = getContext >>= pure . getter . _primInfo

-- -- | Retrieves a type from the context. If the type doesn't exist it fails
-- getType :: Stream s m Char => String -> ParsecT s u m (Maybe Type)
-- getType s = do
--   context <- getState
--   let alias = fromMaybe s (Map.loopkup s (_typeAliases context))
--   pure $ Map.lookup alias (_types context)

-- intTypeOf :: Stream s m Char => (PrimeIntInfo ->) -> ParsecT s u m (Maybe Type)

-- -- | Retrieves the type of any litteral integer
-- --
-- -- According to the C99 standard (6.4.4.1), hexadecimal constants will be
-- -- the first type on this list that can represent them:
-- --
-- -- int, unsigned int, long int, unsigned long int, long long int, unsigned
-- -- long long int
-- --
-- -- Intenrally we do not care exactly what the name some integer types
-- -- matches. This will just return the most primitive type signateure possible of
-- -- an the integer matching the input value
-- integerFit :: Stream s m Char => String -> ParsecT s u m (Maybe PrimInt)
-- integerFit num =  do
--   pi <- _primeInfo <$> getState
--   let candidateFs = [ _int
--                     , _long
--                     , _uLong
--                     , _longLong
--                     , _uLongLong
--                     ]
--       mMatch = find (\ f -> _maxValue (f pi) <= num) candidateFs
--   pure $ intInfoType <$> mMatch


-- -- | Retieves a lexeme matching a specific function. If the function returns
-- -- false the parser fails consuming the input.
-- lexMatching :: (Monad m, Stream s Identity (Lexeme String)) => (String -> Bool) -> ParsecT s CContext m String
-- lexMatching f = token show _sourcePos matcher
--   where
--     matcher t = if f t^.tokenValue then Just (t^.tokenValue) else Nothing

-- -- | Parses a lexeme maching a specific string. If the strings doesn't match the
-- -- parser fails consuming the input
-- string :: (IsString str, Stream s m Char) => str -> ParsecT s u m String
-- string s = lexMatching (==fromString s)

-- -- | Parses any lexeme
-- anyLexeme :: Stream s m Char => ParsecT s u m String
-- anyLexeme = lexMatching $ const True

-- -- | Parses a lexeme from the stream by applying a function yeilding a maybe
-- -- value. If the function returns nothing the function fails consuming the input
-- lexFromMaybe :: (Monad m, Stream s Identity (Lexeme String)) => (String -> Maybe a) -> ParsecT s CContext m a
-- lexFromMaybe f = do
--   inner <- anyLexme
--   maybe (fail "Didn't match inner") pure inner

-- -- | Parses one token matching some of the string given as arguments. If none
-- -- does the parsr fails consuming the input
-- oneOf :: (Stream s m Char) => [String] -> ParsecT s u m String
-- oneOf elems = lexMatching (`elem` map fromString elems)

-- -- | Parses any token that can't be found in the given list
-- noneOf :: (Stream s m Char) => [String] -> ParsecT s u m String
-- noneOf elems = lexMatching (\ e -> not $ elem e elems)

-- semicolon :: Stream s m Char =>  ParsecT s u m String
-- semicolon = string ";"

-- colon :: Stream s m Char =>  ParsecT s u m String
-- colon = string ":"

-- dot :: Stream s m Char =>  ParsecT s u m String
-- dot = string "."

-- comma :: Stream s m Char => ParsecT s u m String
-- comma = string ","

-- -- | Perses the '='  operator
-- assignmentOp :: Stream s m Char => ParsecT s u m String
-- assignmentOp = string "=="

-- leftParen :: Stream s m Char =>  ParsecT s u m String
-- leftParen = string "("

-- rightParen :: Stream s m Char =>  ParsecT s u m String
-- rightParen = string ")"

-- leftBracket :: Stream s m Char =>  ParsecT s u m String
-- leftBracket = string "["

-- rightBracket :: Stream s m Char =>  ParsecT s u m String
-- rightBracket = string "]"

-- leftCurlyBracket :: Stream s m Char => ParsecT s u m String
-- leftCurlyBracket = string "{"

-- rightCurlyBracket :: Stream s m Char => ParsecT s u m String
-- rightCurlyBracket = string "}"

-- ref :: Stream s m Char => ParsecT s u m String
-- ref = string "&"

-- astersik :: Stream s m Char => ParsecT s u m String
-- astersik = string "*"

-- -- | Parses the const keyword
-- constQualifier :: Stream s m Char => ParsecT s u m String
-- constQualifier = string "const"

-- -- | Parses the static keyword
-- staticQualifier :: Stream s m Char => ParsecT s u m String
-- staticQualifier = string "static"

-- -- | Parses the extern keyword
-- externalQualifier :: Stream s m Char => ParsecT s u m String
-- externalQualifier = string "extern"

-- -- | Parses an some kind of integer (decimal or hex) and gives its numerical
-- -- value as well as its interpreted type. The type is important for the correct
-- -- semantics of numerical expression.
-- integerLit :: Stream s m Char => ParsecT CContext u m (PrimInt, Integer)
-- integerLit = try decimal  <|>  try hex <|> octal
--   where
--     intTypeOf getter = intInfoType $ getter basePrimInfo
--     hex = do
--       num <- parseFromMaybe parseHexaDec
--       fit <- integerFit num
--       pure (intTypeOf fit, num)
--     octal = do
--       _
--     deicmal = do
--       lex <- anyLexeme
--       let mDec = readMaybe (takeWhile Char.isDigit lex)
--       num <- maybe (fail "Invalid decimal integer given") pure mDec
--       let typePart = dropWhile Char.isDigit lex
--       case typePart of
--         [] -> do
--           fit <- integerFit num
--           (fit, num)
--         suffix -> do
--           primGetter <- maybe (fail "Invalid suffix given") pure (suffixToPrimGetter suffix)
--           intType <- intInfoType <$> getPrimInt primGetter
--           pure (intType, num)

--     suffixToPrimGetter "u" = pure _unsigned
--     suffixToPrimGetter "l" = pure _long
--     suffixToPrimGetter "ul" = pure _uLong
--     suffixToPrimGetter "ll" = pure _longLong
--     suffixToPrimGetter "ull" = pure _uLongLong
--     suffixToPrimGetter _ = fail "Invalid integer suffix given"

-- -- | Parses a float litteral together with its datatype. The scientific return
-- -- datatype is to enure that the there's no precision loss. If the lexeme isn't
-- -- a floating point type the parser fails and consumes the input.
-- float :: Stream s m Char => ParsecT CContext u m (PrimFloat, Scientific)
-- float = do
--   intStr <- anyLexeme
--   sorceName <- getSourceName
--   (suffixString, num) <- either (fail . show) pure (parse digitParse sourceName intStr)
--   primFloat <- getPrimeFloat =<< suffixToPrimGetter suffixString
--   pure (primFloat,
--   where
--     suffixToPrimGetter "" = pure _float
--     suffixToPrimGetter "f" = pure _float
--     suffixToPrimGetter "d" = pure _double
--     suffixToPrimGetter "l" = pure _longDouble
--     suffixToPrimGetter _ = fail

--     digitParse = do
--       preDot <- many (try digit)
--       dot <- fromMaybe "" <$> optionMaybe (trt $ PG.char '.')
--       postDot <- many (try digit)
--       exp <- fromMaybe "" <$> optionMaybe (try $ oneOf "eE")
--       expSign <- fromMaybe "" <$> optionMaybe (try $ oneOf "+-")
--       expNum <- many (try digit)
--       typeSuffix <- many anyToken
--       let digitString = preDot <> dot <> postDot <> exp <> expSign <> expNum
--       num <- maybe (failed "Invalid floating point given") pure (readMaybe digitString)
--       pure (typeSuffix, num)

-- -- | Generally parse something that can be considerad an identifier (type, variable or keyword)
-- identifier :: (IsString t, Stream s m Char) => ParsecT s u m t
-- identifier = fromString <$> isIdentifier

-- -- | Parses an array qualifier such as "[]" or "[512]". In case there's no size
-- -- qualifier Nothing is returned, otherwise the represented number
-- arrayQualifier :: Stream s m Char => ParsecT s u m (Maybe Word64)
-- arrayQualifier = do
--   leftBracket
--   mCount <- optionMaybe (try integer)
--   rightBracket
--   pure $ fromIntegral <$> mCount

-- -- | Parses a type name to a type, This returned type may be a pre defined
-- -- composite kind but it does not parse pointers or array types
-- typename :: Stream s m Char => ParsecT s u m (TypeName, Type)
-- typename = do
--   ps <- getParserState
--   combinedType "" ps
--   where
--     combinedType acc ps = do
--         identifier <- labelname
--         let combinedId = acc <> identifier
--         case Map.lookup combinedId (_types ps) of
--           Nothing -> fail "Typename does not exist at point"
--           Just t -> try (combinedType (combinedId <> " ") ps)
--                  <|> pure (combinedId, t)

-- -- | Only parses the typseign of some type that is for example "const int *
-- -- const *"
-- typeSign :: Stream s m Char => ParsecT s u m Type
-- typeSign = do
--   -- | Can be many consts but these doesn't matter
--   hasConst <- doesParse $ (many (try constQualifier))
--   (_,typename) <- try typename
--   ptrs <- many (try (asterisk >> pure PtrT) <|> try (constantQualifider >> ConstT))
--   -- | the constant operator is right that is we apply it from right to left eg
--   -- int * const is a (constant pointer) to an int except for a the possible
--   -- first constant such as const int * which is (Pointer (Constant int))
--   let ptrify = foldl' (\ accF f  -> f accF) id ptrs
--       startConst = if hasConst then ConstT else id
--   pure $ deConstT $ startConst $ ptrify typename
--   where
--     constPtrP = do
--       constQualifier
--       astersik
--       pure ConstT
--     deConstT (ConstT (ConstT a)) = deconst (ConstT a)
--     deConstT (f a) = f (deConstT a)

-- -- | Parses the type signature of a function expression
-- funcionDefSign :: Stream s m Char => ParsecT s u m FunctionDefSign
-- funcionDefSign = do
--   isStatic <- doesParse $ staticQualifier
--   returnType <- typeSign
--   funName <- identifier
--   leftParens
--   arguments <- argList
--   rightParens
--   let ty = Function (fmap (\(ArgDecl (VarDecl _ t)) -> t) arguments) returnType
--   pure $ FunctionDefSign isStatic funName returnType arguments

-- -- | Parses the type signature of a function expression
-- funcionDeclSign :: Stream s m Char => ParsecT s u m FunctionDeclSign
-- funcionDeclSign = do
--   try (externalQualifier)
--   returnType <- typeSign
--   funcName <- identifier
--   leftParens
--   arguments <- declArgs
--   rightParens
--   let ty = Function $  (extractTypes arguments) returnType
--       args = fmap (either (\ t -> Just t) (const Nothing)) a arguments
--   pure $ FunctionDeclSign funcName ty args

--   where
--     extractTypes = fmap (either (\case  (VarName _ t) -> t) id)
--     extractType (Left t) = t
--     extractType (Right (ArgDecl (VarSign _ t))) = t
--     declArgs = do
--       def <- (try (Left <$> varDef) <|> (Right <$> typeSign))
--       parsedComma <- doesParse $ try comma
--       if parsedComma then (def:) <$> declArgs else pure [def]

-- -- | Parses an argument declatation
-- argDecl :: Stream s m Char => ParsecT s u m ArgDecl
-- argDecl = ArgDecl <$> varSign

-- -- | Parses an argument list not including the paranthesis
-- -- Example: "int x, char c"
-- argList :: Stream s m Char => ParsecT s u m [ArgDecl]
-- argList = parseLoop
--   where
--     parseLoop = do
--       def <- argDecl
--       parsedComma <- doesParse $ try comma
--       if parsedComma then (def:) <$> loop else pure [def]

-- -- | Parses a variable defeinition purely, this does not include variable
-- -- delarations or static (that is prefixed with extern) and does not parse the
-- -- initalizer. Eg it can parse stuf as "const int* const * x".
-- varSign :: Stream s m Char => ParsecT s u m VarSign
-- varSign = do
--   ts <- typeSign
--   name <- identifier
--   mArrayQualifier <- optionMaybe (try $ arrayQualifier)
--   let finalType =
--         case mArrayQualifier of
--           Just (Just len) -> Array len ts
--           Just Nothing -> Ptr ts
--           Nothing -> ts
--   VarSignDef name finalType


-- -- | A varialbe declaration is simply a variable sign prefixed with an "extern"
-- varDecl :: Stream s m Char => ParsecT s u m VarDecl
-- varDecl = constQualifier >> varSign


-- varDef :: Stream s m Char => ParsecT s u m VarDef
-- varDef =
--   VarDef <$> varSign <*> optionMaybe (try $ assignmentOp >> expression)


-- expression :: Stream s m Char => ParsecT s u m VarDef
-- expression = _
