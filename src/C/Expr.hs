{-# LANGUAGE GADTs #-}

module C.Expr where

-- import Data.Text (Text)
-- import C.Types

-- import Data.Word
-- import Data.Int

-- import Data.Map (Map)

-- -- | Not used directly in types but as a tag for litterals such that constant
-- -- expressions can be correctly calculated
-- data IntLitType where
--   SIntType :: Word16 -> IntLitType
--   UIntType :: Word16 -> IntLitType
--   deriving (Show, Read, Eq)

-- data IntLitValue where
--   IntLitValue :: IntLitType -> Integer -> IntLitValue
--   deriving (Show, Read, Eq)

-- data FloatLitValue where
--   FloatLitValue :: IntLitType -> Integer -> IntLitValue
--   deriving (Show, Read, Eq)

-- data Litteral where
--   StringLit :: Text -> Litteral
--   IntLit :: IntLitValue -> Litteral
--   FloatLit :: FloatLitValue -> Litteral
--   ArrayLit :: [Expression] -> Litteral
--   deriving (Show, Read, Eq)

-- -- | All binary operators defined in the C language
-- data BinaryOperator where
--   Plus :: BinaryOperator
--   Minus :: BinaryOperator
--   Product :: BinaryOperator
--   Divide :: BinaryOperator
--   Remaninder :: BinaryOperator
--   Xor :: BinaryOperator
--   And :: BinaryOperator
--   Or :: BinaryOperator
--   Equal :: BinaryOperator
--   deriving (Show, Read, Eq)

-- newtype Block = Block [Expression]
--   deriving (Show, Read , Eq)
-- -- | C expressions not assosiated with top level. Binary assignment operators
-- -- are semantically a combination of a binary operator and the assign ment
-- -- operator and are parsed as such.
-- data Expression where
--   LabelExp :: LabelName -> Expression
--   LiteralExp :: Litteral -> Expression
--   -- | A Block expression whose sole purpose is to
--   BlockExp :: Block -> Expression
--   DefExp :: VarDef -> Expression
--   ConstDefExp :: VarDef -> Expression
--   StaticDefExp :: VarDef -> Expression
--   AssignExp :: LabelName -> Expression -> Expression
--   -- | Collective binary operator expression
--   OperatorExp :: BinaryOperator -> Expression -> Expression -> Expression
--   -- | Not fully correct
--   ReferenceExp :: LabelName -> Expression
--   NegateExp :: Expression -> Expression
--   DerefExp :: Expression -> Expression
--   ReturnExp :: Expression -> Expression
--   -- | init, check and step and body
--   ForExp :: Expression -> Expression -> Expression -> Expression -> Expression
--   -- | Check and body,
--   WhileExp :: Expression -> Expression -> Expression
--   -- | Type case of expr to type
--   CastExp :: Type -> Expression -> Expression
--   deriving (Show, Read, Eq)

-- -- | Sign part of a variable EG: "const int x[]"
-- data VarSign where
--   VarSign :: VarName -> Type -> VarDef
--   deriving (Show, Read, Eq)

-- -- | A variable definition is a sign
-- -- together an optional initializer
-- data VarDef where
--   VarDef :: VarSign -> Maybe Expression -> VarDef
--   deriving (Show, Read, Eq)

-- newtype VarDecl where
--   VarDecl :: VarSign -> VarDecl
--   deriving (Show,Read, Eq)
-- -- | Arg defs are just variable signatures that are turned into
-- -- variables uppon function invokation
-- newtype ArgDecl = ArgDecl VarSign
--   deriving (Show, Read, Eq)


-- -- | Defation of a type declaration
-- data TypeDefDef = TypeDefDef LabelName Type
--   deriving (Show, Read, Eq)

-- -- | signs of functions that externally defined. The difference to function
-- -- defenitions is that the declaration does not have a body and the argument
-- -- names are optional
-- data FunctionDeclSign =
--   FunctionDeclSign
--   { _name   :: FunctionName
--   , _type      :: FunctionType
--   -- | Argument names does not necisarily have to exist for declaratons, those
--   -- that doesn't have a name is denoted with a 'Nothing'. Not that we normally
--   -- care?
--   , _arguments :: [Maybe ArgDef]
--   } deriving (Show, Read, Eq)

-- -- | Contains all data that should be known about a full function defenition
-- -- That is the sign of a function that is also defined on spot
-- data FunctionDefSign =
--   FunctionDefSign
--   { _isStatic :: Bool
--   , _name   :: FunctionName
--   , _type      :: FunctionType
--   -- | Must contain the same types in the same order of the function type
--   , _arguments :: [ArgDef]
--   } deriving (Show, Read, Eq)



-- -- | Top level epressions, only allowed to be variable declarations. It could be
-- -- assumed that the c pre processor has allready been run here
-- data TopExpression where
--   -- | Defarations of types, Labelname is the label given to the given type
--   TypeDefTE :: LabelName -> Type -> TopExpression
--   -- | Top const var declr are ment for global
--   TopVarDeclTE :: VarDef -> TopExpression
--   -- | Variable defenition
--   TopVarDefTE :: VarDef -> TopExpression
--   -- | Definition of functions
--   TopFunDefTE :: FunctionDef -> TopExpression
--   -- | Declaration of functions.
--   TopFunDeclTE :: FunctionDef -> TopExpression
