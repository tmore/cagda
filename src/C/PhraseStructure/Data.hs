-- | Author: Tomas Möre 2019
--
-- This definition of expression differs slightly from the C standard
-- defenition. As the C standad grammar encapsulates the parsing techinqes. This
-- is however defined for simplicity this means that any constilation of data
-- defeinition may not be a valid C program. However, this is not true for the
-- original grammar either.
--
-- Any extra constrains should be checked afterwards
--
-- The goal here is that it should be possible to lose any important information
-- when parsing and decoding a valid c file. However, since this may not be of
-- interest to the work others are doing the idea is to utilize the annotation
-- space available at most levels of the syntax tree. During the parsing in this
-- library we will associate all comments that may be related to any particular
-- term in the annotation together with the original files position. Here, we
-- say that a comment is associated with whatever term comes after it.
--
-- This part ignores all semantic meaning of the underlying expression even
-- looser then the original C defenition does (For example constant expressions
-- are simply expresson etc)

{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE RecordWildCards #-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module C.PhraseStructure.Data where

import C.PhraseStructure.Util

import C.Token.Constant -- Import should b removed and handled properly at some time
import C.Token.String


import Data.List.NonEmpty

-- | Independant constant defenition
data Literal =
   CConst Constant
  | StringConst StringLiteral
  deriving (Show, Read, Eq)

-- | (6.5.1)
-- a is anything that we may want to store in association with some node. (Source position, tags / whatever)
-- i is the type of the identifiers

data Expression a i =
  -- | expression , assignment-expression
  ParensE
  { _annotation :: a
  , _expression :: Expression a i -- ^ Inner expression
  }

  -- 6.5.17
  | CommaSeparatedE
    { _annotation :: a
    , _effectExpression :: Expression a i
    , _retrunExpression :: Expression a i
    }
  -- (6.5.1)
  | IdentifierE
    { _annotation :: a
    , _symbol :: i
    }
  | ConstantE
    { _annotation :: a
    , _value :: Literal
    }
  | GenericE
    { _annotation :: a
    , _genericSelection :: GenericSelection a i
    }

  -- (6.5.2) Postfix
  -- | All posfix does a bit of backwards recursion, to simplify this step the
  -- backwards part is handeled on this level and the postfix part is handeled
  -- by 'PostfixExpression'
  | PostfixE
    { _annotation :: a
    , _expression :: Expression a i
    , _postFixExpr :: PostfixExpression a i
    }
  -- | (typeName){ initialiser list }
  | InitializerE
    { _annotation :: a
    , _type :: TypeName a i
    , _initialisers :: [Initializer a i ]
    }
  -- (6.5.3) Unary
  -- | ++expr
  | UnaryOpE
    { _annotation :: a
    , _unaryOp :: UnaryOperator
    , _expression :: Expression a i
    }
  -- | sizeof unary-epxression
  | ExpressionSizeofE
      { _annotation :: a
      , _expression :: Expression a i
      }
  -- | sizeof ( type-name )
  | TypeSizeofE
    { _annotation :: a
    , _type :: TypeName a i
    }
  -- | _Alignof ( type-name )
  | AlignOfE
    { _annotation :: a
    , _type :: TypeName a i
    }
  -- | (type-name) expression
  | CastE
    { _annotation :: a
    , _type :: TypeName a i
    , _expression :: Expression a i
    }
  -- | All binary operations including assignment
  -- expr <op> expr
  | BinaryOpE
    { _annotation :: a
    , _binOp :: BinaryOperator
    , _leftExpression :: Expression a i
    , _rightExpression :: Expression a i
    }
  -- | expr1 ? expr2 : expr3
  | TernaryIfE
    { _annotation :: a
    , _guardExpression
    , _trueCaseExpression :: Expression a i
    , _falseCaseExpression :: Expression a i
    }

deriving instance (Eq a, Eq i) => Eq (Expression a i)
deriving instance (Show a, Show i) => Show (Expression a i)
deriving instance (Read a, Read i) => Read (Expression a i)

instance HasAnnotation (Expression a i) a where
  annotationOf = _annotation

data PostfixExpression a i =
  -- | [ expr]
  ArraySubscriptE
  { _annotation :: a
  , _expression :: Expression a i
  }
  -- | ( expr* )
  | MethodCallE
    { _annotation :: a
    , _paramExpressions :: [Expression a i]
    }
  -- | . identifier
  | StructAccessE
    { _annotation :: a
    , _fieldSymbol :: i
    }
  -- |  -> identifier
  | DerefStructAccessE
    { _annotation :: a
    , _fieldSymbol :: i
    }
  -- | ++
  | PostIncrementE
    { _annotation :: a
    }
  -- | postfixExpr--
  | PostDecrementE
    { _annotation :: a }


deriving instance (Eq a, Eq i) => Eq (PostfixExpression a i)
deriving instance (Show a, Show i) => Show (PostfixExpression a i)
deriving instance (Read a, Read i) => Read (PostfixExpression a i)

instance HasAnnotation (PostfixExpression a i) a where
  annotationOf = _annotation


data UnaryOperator where
  -- | ++
  PreIncrement :: UnaryOperator
  -- | --
  PreDecrement :: UnaryOperator
  -- | &
  ToRef :: UnaryOperator
  -- | *
  Deref :: UnaryOperator
  -- | +
  Positive :: UnaryOperator
  -- | Negate
  Negative :: UnaryOperator
  -- | ~
  BitFlip :: UnaryOperator
  -- | !
  Not :: UnaryOperator
  deriving (Show, Read, Eq)

-- | (6.5.5) - (6.5.16)

-- | All binary operators defined in the C language
data BinaryOperator where
  Plus        :: BinaryOperator
  Minus       :: BinaryOperator
  Multiply    :: BinaryOperator
  Divide      :: BinaryOperator
  LeftShift   :: BinaryOperator
  RightShift  :: BinaryOperator
  Remaninder  :: BinaryOperator
  BitwiseXor  :: BinaryOperator
  BitwiseAnd  :: BinaryOperator
  BitwiseOr   :: BinaryOperator
  LogicalAnd  :: BinaryOperator
  LogicalOr   :: BinaryOperator
  Equal       :: BinaryOperator
  NotEqual    :: BinaryOperator
  LessThen    :: BinaryOperator
  GreaterThen :: BinaryOperator
  LessEqualThen    :: BinaryOperator
  GreaterEqualThen :: BinaryOperator

  -- | I count assignment operators as binary operators because they do return a
  -- value.
  Assign           :: BinaryOperator
  MuliplyAssign    :: BinaryOperator
  DivideAssign     :: BinaryOperator
  RemainderAssign  :: BinaryOperator
  PlusAssign       :: BinaryOperator
  MinusAssign      :: BinaryOperator
  LeftShiftAssign  :: BinaryOperator
  RightShiftAssign :: BinaryOperator
  BinaryAndAssign  :: BinaryOperator
  BinaryXorAssign  :: BinaryOperator
  BinaryOrAssign   :: BinaryOperator
  deriving (Show, Read, Eq)

-- | (6.5.1.1)
data GenericSelection a i =
  -- | _Generic ( assignment-expression , generic-assoc-list )
  GenericSelection
  { _annotation :: a
  , _assignmentExpression :: Expression a i
  , _assocList :: [GenericAssociation a i]
  }

deriving instance (Eq a, Eq i) => Eq (GenericSelection a i)
deriving instance (Show a, Show i) => Show (GenericSelection a i)
deriving instance (Read a, Read i) => Read (GenericSelection a i)
instance HasAnnotation (GenericSelection a i) a where
  annotationOf = _annotation

  -- | (6.5.1.1)
data GenericAssociation a i =
  -- | type-name : assignment-expression
  CustomAssoc { _annotation :: a
              , _typeName :: TypeName a i
              , _expression :: Expression a i
              }
  -- | default : assignment-expression
  | DefaultAssoc { _annotation :: a
                 , _expression :: Expression a i
                 }

deriving instance (Eq a, Eq i) => Eq (GenericAssociation a i)
deriving instance (Show a, Show i) => Show (GenericAssociation a i)
deriving instance (Read a, Read i) => Read (GenericAssociation a i)
instance HasAnnotation (GenericAssociation a i) a where
  annotationOf = _annotation


newtype TypeName a i where
  TypeName :: [SpecifierOrQualifier a i] -> TypeName a i

deriving instance (Eq a, Eq i) => Eq (TypeName a i)
deriving instance (Show a, Show i) => Show (TypeName a i)
deriving instance (Read a, Read i) => Read (TypeName a i)

data SpecifierOrQualifier a i =
  Specifier
  { _annotation :: a
  , _specifier :: TypeSpecifier a i
  }
  | Qualifier
    { _annotation :: a
    , _qualifier :: TypeQualifier
    }

deriving instance (Eq a, Eq i) => Eq (SpecifierOrQualifier a i)
deriving instance (Show a, Show i) => Show (SpecifierOrQualifier a i)
deriving instance (Read a, Read i) => Read (SpecifierOrQualifier a i)
instance HasAnnotation (SpecifierOrQualifier a i) a where
  annotationOf = _annotation

-- DECLARATIONS
data Declaration a i =
  -- | declaration-specifiers init-declarator-list_opt ;
  -- init-declarator-list:
  --     init-declarator
  --     init-declarator-list , init-declarator
  -- init-declarator:
  --     declarator
  --     declarator = initializer
  NormalDeclaration
  { _annotation :: a
  , _specifiers :: [DeclarationSpecifier a i]
  , _assignments :: [(Declarator a i, Initializer a i)]
  }
  -- | _Static_assert ( constant-expression , string-literal ) ;
  | StaticAssertDecl
    { _annotation :: a
    , _expression :: Expression a i
    , _message :: String
    }

deriving instance (Eq a, Eq i) => Eq (Declaration a i)
deriving instance (Show a, Show i) => Show (Declaration a i)
deriving instance (Read a, Read i) => Read (Declaration a i)
instance HasAnnotation (Declaration a i) a where
  annotationOf = _annotation

data DeclarationSpecifier a i =
  StorageClassSpecifierDS
  { _annotation :: a
  , _storageSpecifier :: StorageClassSpecifier
  }
  | TypeSpecifierDS
    { _annotation :: a
    , _typeSpecifier :: TypeSpecifier a i
    }
  | TypeQualifierDS
    { _annotation :: a
    , _qualifier :: TypeQualifier
    }
  | FunctionSpecifierDS
    { _annotation :: a
    , _functionSpecifier :: FunctionSpecifier
    }
  | AlignmentSpecifierDS
    { _annotation :: a
    , _alignMentSpecifier :: AlignmentSpecifier a i
    }

deriving instance (Eq a, Eq i) => Eq (DeclarationSpecifier a i)
deriving instance (Show a, Show i) => Show (DeclarationSpecifier a i)
deriving instance (Read a, Read i) => Read (DeclarationSpecifier a i)
instance HasAnnotation (DeclarationSpecifier a i) a where
  annotationOf = _annotation


data StorageClassSpecifier where
  TypeDef :: StorageClassSpecifier
  Extern :: StorageClassSpecifier
  Static :: StorageClassSpecifier
  ThreadLocal :: StorageClassSpecifier
  Auto :: StorageClassSpecifier
  Register :: StorageClassSpecifier
  deriving (Show, Read, Eq)

-- | The annotation of specifiers should come from the wrapping context
data TypeSpecifier a i where
  Void :: TypeSpecifier a i
  Char :: TypeSpecifier a i
  Short :: TypeSpecifier a i
  Int ::  TypeSpecifier a i
  Long :: TypeSpecifier a i
  Float :: TypeSpecifier a i
  Double :: TypeSpecifier a i
  Signed :: TypeSpecifier a i
  Unsigned :: TypeSpecifier a i
  Bool :: TypeSpecifier a i
  Complex :: TypeSpecifier a i
   -- _Atomic ( type-name )
  Atomic :: TypeName a i -> TypeSpecifier a i
  Struct :: StructSpecifier a i -> TypeSpecifier a i
  Union  :: UnionSpecifier a i -> TypeSpecifier a i
  Enum :: EnumSpecifier a i -> TypeSpecifier a i
  TypedefName :: i -> TypeSpecifier a i

deriving instance (Eq a, Eq i) => Eq (TypeSpecifier a i)
deriving instance (Show a, Show i) => Show (TypeSpecifier a i)
deriving instance (Read a, Read i) => Read (TypeSpecifier a i)

data StructSpecifier a i =
  LiteralStruct
  { _annotation :: a
  , _mName :: Maybe i
  , _declarations :: [StructDeclaration a i]
  }
  | NamedStruct
    { _annotation :: a
    , _name :: i
    }

deriving instance (Eq a, Eq i) => Eq (StructSpecifier a i)
deriving instance (Show a, Show i) => Show (StructSpecifier a i)
deriving instance (Read a, Read i) => Read (StructSpecifier a i)
instance HasAnnotation (StructSpecifier a i) a where
  annotationOf = _annotation

data StructDeclaration a i =
  StructStaticAsseret
  { _annotation :: a
  , _declaration :: [Declarator a i]
  }
  | StructDeclaration
    { _annotation :: a
    , _declaration :: [Declarator a i]
    , _expression :: Expression a i
    }

deriving instance (Eq a, Eq i) => Eq (StructDeclaration a i)
deriving instance (Show a, Show i) => Show (StructDeclaration a i)
deriving instance (Read a, Read i) => Read (StructDeclaration a i)
instance HasAnnotation (StructDeclaration a i) a where
  annotationOf = _annotation


data UnionSpecifier a i =
  LiteralUnion
  { _annotation :: a
  , _mName ::  Maybe i
  , _declarations :: [StructDeclaration a i]
  }
  | NamedUnion
    { _annotation:: a
    , _name :: i
    }


deriving instance (Eq a, Eq i) => Eq (UnionSpecifier a i)
deriving instance (Show a, Show i) => Show (UnionSpecifier a i)
deriving instance (Read a, Read i) => Read (UnionSpecifier a i)
instance HasAnnotation (UnionSpecifier a i) a where
  annotationOf = _annotation


data EnumSpecifier a i =
  LiteralEnum
  { _annotation :: a
  , _mName :: Maybe i
  , _enumerators :: [Enumerator a i]
  }
  | NamedEnum
    { _annotation :: a
    , _name :: i
    }

deriving instance (Eq a, Eq i) => Eq (EnumSpecifier a i)
deriving instance (Show a, Show i) => Show (EnumSpecifier a i)
deriving instance (Read a, Read i) => Read (EnumSpecifier a i)
instance HasAnnotation (EnumSpecifier a i) a where
  annotationOf = _annotation


data Enumerator a i =
  -- | enumeration-constant
  Enumerator
  { _annotation :: a
  , _name :: i
  , _mValue :: Maybe (Expression a i)
  }

deriving instance (Eq a, Eq i) => Eq (Enumerator a i)
deriving instance (Show a, Show i) => Show (Enumerator a i)
deriving instance (Read a, Read i) => Read (Enumerator a i)
instance HasAnnotation (Enumerator a i) a where
  annotationOf = _annotation

data TypeQualifier =
  -- const
  ConstQualifier
  -- restict
  | RestrictQualifier
  -- volatile
  | VolatileQuaifier
  -- _Atomic
  | AtomicQualifier
  deriving (Show, Read, Eq)


data FunctionSpecifier where
  -- inline
  Inline :: FunctionSpecifier
  -- _Noretrun
  Noreturn :: FunctionSpecifier
  deriving (Show, Read, Eq)


data AlignmentSpecifier a i =
  AlignasTypeName
  { _type :: TypeName a i
  }
  | AlignAsConstExpr
    { _name :: TypeName a i
    }

deriving instance (Eq a, Eq i) => Eq (AlignmentSpecifier a i)
deriving instance (Show a, Show i) => Show (AlignmentSpecifier a i)
deriving instance (Read a, Read i) => Read (AlignmentSpecifier a i)

-- | Pointers 6.7.6
data Pointer a =
  Pointer
  { _annotation :: a
  -- | the qualifier list comes in tuples of annotation and actual qualifiers.
  -- While less confinient at this particular spot it simplifies the logic more
  -- at other places
  , _qualifierList :: [(a, TypeQualifier)]
  , _mPointer :: Maybe (Pointer a)
  }

deriving instance (Eq a) => Eq (Pointer a)
deriving instance (Show a) => Show (Pointer a)
deriving instance (Read a) => Read (Pointer a)
instance HasAnnotation (Pointer a) a where
  annotationOf = _annotation

data Declarator a i =
  -- | Pointer lacks annotation at this point since it is needed recursively
  PointerDeclarator
  { _pointer :: Pointer a
  , _declarator :: Declarator a i
  }
  -- | Some idetifier
  | DeclIndetifier
    { _annotation :: a
    , _symbol :: i
    }
  -- | ( declarator )
  | DeclParens
    { _annotation :: a
    , _declarator :: Declarator a i
    }
  -- I do not know what to call these yet

  -- | direct-declarator [ stuff (See array declarator) ]
  | ArrayDecl
    { _annotation :: a
    , _leftDeclarator :: Declarator a i
    , _arrayDeclarator :: ArrayDeclarator a i
    }
  -- | direct-declarator ( parameter-type-list )
  | ParametersDecl
    { _annotation :: a
    , _leftDeclarator :: Declarator a i
    , _paramters :: NonEmpty (ParameterDeclaration a i)
    }
  -- | direct-declarator ( identifier-list_opt )
  | IdentifierListDecl
    { _annotation :: a
    , _leftDeclarator :: Declarator a i
    , _identifiers :: [i]
    }

deriving instance (Eq a, Eq i) => Eq (Declarator a i)
deriving instance (Show a, Show i) => Show (Declarator a i)
deriving instance (Read a, Read i) => Read (Declarator a i)
instance HasAnnotation (Declarator a i) a where
  annotationOf PointerDeclarator{_pointer = pointer} = annotationOf pointer
  annotationOf a = (_annotation :: Declarator a i -> a) a

data ArrayDeclarator a i =
  -- |  [ type-qualifier-list_opt assignment-expression_opt ]
  BasicArrayDecl
  { _qualifiers :: [TypeQualifier]
  , _mExpression ::  Maybe (Expression a i)
  }
  -- |  [ static type-qualifier-list_opt assignment-expression ]
  | StaticArrayDecl
  { _qualifiers :: [TypeQualifier]
  , _expression :: Expression a i
  }
  -- | type-qualifier-list static assignment-expression
  | StaticAssignmentArrayDecl
  { _qualifiers :: [TypeQualifier]
  , _expression :: Expression a i
  }

deriving instance (Eq a, Eq i) => Eq (ArrayDeclarator a i)
deriving instance (Show a, Show i) => Show (ArrayDeclarator a i)
deriving instance (Read a, Read i) => Read (ArrayDeclarator a i)
-- instance HasAnnotation (ArrayDeclarator a i) a where
--   annotationOf = _annotation

data ParameterDeclaration a i =
  ConcreteParameterDecl
  { _annotation :: a
  , _specifiers :: NonEmpty (DeclarationSpecifier a i)
  , _declarator :: Declarator a i
  }
  -- Leaving out abstract declarators for now
  -- AbstractParameterDecl :: [DeclarationSpecifier] -> Maybe AbstractDeclarator -> ParameterDeclaration

deriving instance (Eq a, Eq i) => Eq (ParameterDeclaration a i)
deriving instance (Show a, Show i) => Show (ParameterDeclaration a i)
deriving instance (Read a, Read i) => Read (ParameterDeclaration a i)
instance HasAnnotation (ParameterDeclaration a i) a where
  annotationOf = _annotation


data Initializer a i =
  -- | assignment-expression
  AssinmentInitializer (Expression a i)
  -- | { initializer-list }
  --   { initializer-list , }
  -- initializer-list:
  --     designation opt initializer
  --     initializer-list , designation_opt initializer
  --
  -- designator-list:
  --     designator
  --     designator-list designator
  | ListInitializer [(Maybe (Designator a i), Initializer a i )]

deriving instance (Eq a, Eq i) => Eq (Initializer a i)
deriving instance (Show a, Show i) => Show (Initializer a i)
deriving instance (Read a, Read i) => Read (Initializer a i)


data Designator a i =
  -- | [ constant-expression ]
  ContantDesignation [ Expression a i ]
  | IdentifierDesignator i

deriving instance (Eq a, Eq i) => Eq (Designator a i)
deriving instance (Show a, Show i) => Show (Designator a i)
deriving instance (Read a, Read i) => Read (Designator a i)

-- STATEMENTS
data Statement a i =
  LabeledStm
  { _annotation :: a
  , _label :: LabeledStatement a i
  }
  -- | { block-item-list opt }
  | CompoundStm
  { _annotation :: a
  , _blockBody :: [BlockItem a i]
  }
  -- | expression_opt ;
  | ExpressionStm
    { _annotation :: a
    , _expression :: Expression a i
    }
  -- | Specifically for empty statements for easier parsing
  | EmptryExpressionStm
    { _annotation :: a
    }
  -- Unpacked Selection statements
  -- | if ( expression ) statement
  | IfStm
    { _annotation :: a
    , _guard :: Expression a i
    , _trueCase :: Statement a i
    , _falseCase :: Maybe (Statement a i)
    }
  -- | switch ( expression ) statement
  | SwitchStm
    { _annotation :: a
    , _selector :: Expression a i
    , _cases :: Statement a i
    }
  -- Unpacked iteration statements
  -- | while ( expression ) statement
  | WhileStm
    { _annotation :: a
    , _guard :: Expression a i
    , _body :: Statement a i
    }
  -- | do statement while ( expression ) ;
  | DoWhileStm
    { _annotation :: a
    , _body :: Statement a i
    , _guard :: Expression a i
    }
  | ForExprStm
    { _annotation :: a
    , _mInit :: Maybe (Expression a i)
    , _mGuard :: Maybe (Expression a i)
    , _mPostLoop :: Maybe (Expression a i)
    , _body :: Statement a i
    }
  | ForDeclStm
    { _annotation :: a
    , _initDecl :: Declaration a i
    , _mGuard :: Maybe (Expression a i)
    , _mPostLoop :: Maybe (Expression a i)
    , _body :: Statement a i
    }
  -- Unpacked jump statements

  -- | goto identifier ;
  | GotoStm
    { _annotation :: a
    , _labelName :: i
    }
  -- | continue ;
  | ContinueStm
    { _annotation :: a
    }
  -- | break ;
  | BreakStm
    { _annotation :: a
    }
  -- | return expression opt ;
  | ReturnStm
    { _annotation :: a
    , _expression :: Expression a i
    }
  -- | Empty return,
  | VoidReturnStm
    {_annotation :: a
    }

deriving instance (Eq a, Eq i) => Eq (Statement a i)
deriving instance (Show a, Show i) => Show (Statement a i)
deriving instance (Read a, Read i) => Read (Statement a i)
instance HasAnnotation (Statement a i) a where
  annotationOf = _annotation

data BlockItem a i =
  Declaration (Declaration a i)
  | Statement (Statement a i)

deriving instance (Eq a, Eq i) => Eq (BlockItem a i)
deriving instance (Show a, Show i) => Show (BlockItem a i)
deriving instance (Read a, Read i) => Read (BlockItem a i)
instance HasAnnotation (BlockItem a i) a where
  annotationOf (Declaration d) = annotationOf d
  annotationOf (Statement s) = annotationOf s

data LabeledStatement a i =
  -- | identifier : statement
  SimpleLabelStm
    { _annotation :: a
    , _identifier :: i
    , _statement :: Statement a i
    }
  -- | case constant-expression : statement
  | CaseLabelStm
    { _annotation :: a
    , _expression :: Expression a i
    , _stamtement :: Statement a i
    }
  -- | default : statement
  | DefaultLabeledStm
    { _annotation :: a
    , _statement :: Statement a i
    }

deriving instance (Eq a, Eq i) => Eq (LabeledStatement a i)
deriving instance (Show a, Show i) => Show (LabeledStatement a i)
deriving instance (Read a, Read i) => Read (LabeledStatement a i)
instance HasAnnotation (LabeledStatement a i) a where
  annotationOf = _annotation
-- External definitions


newtype TranslationUnit a i = NonEmpty (ExternalDeclaration a i)


data ExternalDeclaration a i =
  FunctionDeclaration (FunctionDefenition a i)
  | DataDeclaration (Declaration a i)

deriving instance (Eq a, Eq i) => Eq (ExternalDeclaration a i)
deriving instance (Show a, Show i) => Show (ExternalDeclaration a i)
deriving instance (Read a, Read i) => Read (ExternalDeclaration a i)
instance HasAnnotation (ExternalDeclaration a i) a where
  annotationOf (FunctionDeclaration f) = annotationOf f
  annotationOf (DataDeclaration d) = annotationOf d

data FunctionDefenition a i =
  FunctionDefenition
  { _annotation :: a
  , _declarationSpecifiers :: NonEmpty (DeclarationSpecifier a i)
  , _declarator :: Declarator a i
  , _declarationList :: [Declaration a i]
  , _compundSatement :: Statement a i
  }

deriving instance (Eq a, Eq i) => Eq (FunctionDefenition a i)
deriving instance (Show a, Show i) => Show (FunctionDefenition a i)
deriving instance (Read a, Read i) => Read (FunctionDefenition a i)
instance HasAnnotation (FunctionDefenition a i) a where
  annotationOf = _annotation
