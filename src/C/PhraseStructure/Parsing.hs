-- | Author: Tomas Möre 2019
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module C.PhraseStructure.Parsing where

import ParserUtils

import C.PhraseStructure.Data

import Text.Megaparsec hiding (Token)
import qualified Text.Megaparsec as MP

import C.Token

import Data.Maybe

-- type MonadPSParsec s m = (MonadParsec PhraseStrutcureError s m
--                          , Stream s, MP.Token s ~ Char
--                          , Eq s)

-- data PhraseStrutcureError where
--   PSError :: PhraseStrutcureError
--   deriving (Show, Read, Eq, Ord)

-- tokenType :: forall a s m . (IsToken a, MonadPSParsec s m) => m a
-- tokenType = do
--   t <- satisfy (tokenIsType @a)
--   case fromToken t of
--     Nothing -> fail "Token not of valid type..."
--     Just a -> pure a

-- matchToken :: (IsToken t, TokenEq t, MonadPSParsec s m) => t -> m t
-- matchToken t = fromJust . fromToken <$> satisfy (tokenContains t)

-- -- | 6.5.1
-- primaryExpression :: MonadPSParsec s m =>  m Expression
-- -- | 6.5.1
-- primaryExpression =
--   choice [ IdentifierE <$> try tokenType
--          , ConstantE <$> try tokenType
--          , StringE <$> try tokenType
--          , GenericE <$> try genericSelection
--          , matchToken LeftParanP *> expression <* matchToken RightParanP
--          ]
-- -- | 6.5.1.1
-- genericSelection :: MonadPSParsec s m =>  m Expression
-- genericSelection = do
--   matchToken GenericKW
--   matchToken LeftParanP
--   assign <- assignmentExpression
--   matchToken CommaP
--   assocList <- genreicAssocList
--   matchToken RightParanP
--   pure $ GenericSelection assign assocList
-- -- | 6.5.1.1
-- genericAssocList :: MonadPSParsec s m =>  m [GenericAssociation]
-- genericAssocList = do
--   ga <- genericAssociation
--   optComma <- optional (try matchToken CommaP)
--   if optComma
--     then (ga :) <$> genericAssocList
--     else pure [ga]
-- -- | 6.5.1.1
-- genericAssociation :: MonadPSParsec s m =>  m GenericAssociation
-- genericAssociation =
--   (try $ CustomAssoc <$> tokenType <*> assignmentExpression)
--   <|> (DefaultAssoc <$> matchToken DefaultKW <*> assignmentExpression)

-- -- | 6.5.2
-- postfixExpression :: MonadPSParsec s m => m Expression
-- postfixExpression =
--   choice [ try primaryExpression
--          , try propperPostfixExpression
--          , initializerExpression
--          ]
--   where
--     -- | These are the experssions that are actually postfix expressions
--     propperPostfixExpression = do
--       prev <- expression
--       postfixPart <- choice [ try arraySubscriptExpression
--                             , try methodCallExpression
--                             , try structAccessExpression
--                             , try defefStructAccessExpression
--                             , try (matchToken IncrementP *> pure PostIncrementE)
--                             , try (matchToken DecrementP *> pure PostDecremetE)
--                             ]
--       pure (PostfixE prev postfixPart)
--     arraySubscriptExpression =
--       ArraySubscritry <$> (matchToken LeftBracketP *> expression <* matchToken RightBracketP)
--     methodCallExpression =
--       MethodCallE <$> (matchToken LeftBracketP *> expression <* matchToken RightBracketP)
--     structAccessExpression =
--       StrucAccessE <$> (matchToken DotP *> try tokenType)
--     defefStructAccessExpression =
--       DerefStructAccess <$> (matchToken ArrowP *> try tokenType)
--     initializerExpression = do
--       matchToken LeftParensP
--       typeName <- matchToken
--       matchToken RightParensP
--       matchToken LeftCurlyBracketP
--       initList <- initalizerList
--       matchToken RightCurlyBracketP
--       pure $ InitializerE typeName (initList <* try (matchToken CommaP))

-- -- | 6.5.2
-- argumentExpressionList :: MonadPSParsec s m => m [Expression]
-- argumentExpressionList = do
--   a <- assignmentExpression
--   optComma <- optional (try (matchToken CommaP))
--   case optComma of
--     Just _ -> (a:) <$> argumentExpressionList
--     Nothing _ -> pure [a]

-- -- | 6.5.3
-- unaryExpression :: MonadPSParsec s m => m Expression
-- unaryExpression = do
--   choice [ try postfixExpression
--          , try $ UnaryOpE <$> unaryOperator <*> unaryExpression
--          , try $ ExpressionSizeofE <$> (matchToken SizeOfKW *> unaryExpression)
--          , try $ TypeSizeOfE <$> (
--              matchToken SizeOfKW *>
--              matchToken LeftParanP *>
--              typeName <*
--              matchToken RightParanP
--              )
--          , AlignOfE <$> (matchToken AlignOfKW *> undefine)
--          ]
-- -- | 6.5.3 (Slightly extended to match ++ and -- as well
-- unaryOperator :: MonadPSParsec s m => m UnaryOperator
-- unaryOperator =
--   choice [ try $ matchToken IncrementP *> pure PreIncrement
--          , try $ matchToken DecrementP *> pure PreDecrement
--          , try $ matchToken AmpersandP *> pure ToRef
--          , try $ matchToken AstersikP *> pure Deref
--          , try $ matchToken PlusP *> pure Positive
--          , try $ matchToken MinusP *> pure Negative
--          , try $ matchToken TildeP *> pure BitFlip
--          , matchToken NotP *> pure Not
--          ]

-- -- | 6.5.4
-- castExpression :: MonadPSParsec s m => m Expression
-- castExpression = try unaryExpression
--               <|> castExpression'
--    where
--      castExpression' = do
--        matchToken LeftParenP
--        tn <- typeName
--        exr <- castExpression
--        pure $ CaseE tn exr

-- -- | Utility function for all binary opperation
-- binOpExpression :: MonadPSParsec s m => m Expression -> m Expression -> BinaryOperator -> Punctuator-> m Expression
-- binOpExpression leftExp rightExp kind sep  = do
--   mp <- leftExp
--   matchToken sep
--   ce <- rightExp
--   pure $ BinaryOp kind mp ce

-- -- | 6.5.5
-- multiplicativeExpression :: MonadPSParsec s m => m Expression
-- multiplicativeExpression = do
--   choice [ try castExpression
--          , try $ mOp Multiply AstersikP
--          , try $ mOp Divide DivideP
--          , parseMult Remaninder RemainderP
--          ]
--   where
--     mOp = binOpExpression multiplicativeExpression castExpression
-- -- | 6.5.6
-- additiveExpression :: MonadPSParsec s m => m Expression
-- additiveExpression =
--   choice [ try multiplicativeExpression
--          , try $ aOp Plus PlusP
--          , aOp Minus MinusP
--          ]
--   where
--     aOp = binOpExpression additiveExpression multiplicativeExpression
-- -- | 6.5.7
-- shiftExpression :: MonadPSParsec s m => m Expressio
-- shiftExpression =
--   choice [ try additiveExpression
--          , try $ sOp LeftShift LeftShiftP
--          , sOp RightShift RightShiftP
--          ]
--   where
--     sOp = binOpExpression shiftExpression additiveExpression
-- -- | 6.5.8
-- relationalExpression :: MonadPSParsec s m => m Expression
-- relationalExpression =
--   choice [ try shiftExpression
--          , try $ rOp LessThen LessP
--          , try $ rOp GreaterThen GreaterP
--          , try $ rOp LessEqualThen LeqP
--          , rOp GreaterEqualThen GeqP
--          ]
--   where
--     rOp = binOpExpression relationalExpression shiftExpression

-- -- | 6.5.9
-- equalityExpression :: MonadPSPArsec s m => m Expression
-- equalityExpression =
--   choice [ try relationalExpression
--          , try $ exOp Equal EqualP
--          , eqOp NotEqual NotEqualP
--          ]
--   where
--     eqOp = binOpExpression equalityExpression relationalExpression
-- -- | 6.5.10
-- andExpression :: MonadPSPArsec s m => m Expression
-- andExpression =
--   try equalityExpression
--   <|> andOp BitwiseAnd AmpersandP
--   where
--     andOp = binOpExpression andExpression equalityExpression

-- -- | 6.5.11
-- exclusiveOrExpression :: MonadPSPArsec s m => m Expression
-- exclusiveOrExpression =
--   try andExpression
--   <|> binOpExpression BitXor BitXorP
--   where
--     xorOp = binOpExpression exclusiveOrExpression andExpression

-- -- | 6.5.12
-- inclusiveOrExpression :: MonadPSPArsec s m => m Expression
-- inclusiveOrExpression =
--   try excluseOrExpression
--   <|> orOp BiwiseOr BitOrP
--   where
--     orOp = binOpExpression inclusiveOrExpression excluseOrExpression

-- -- | 6.5.13
-- logicalAndExpression :: MonadPSPArsec s m => m Expression
-- logicalAndExpression =
--   try inclusiveOrExpression
--   <|> orOp LogicalAnd AndP
--   where
--     andOp = binOpExpression logicalAndExpression inclusiveOrExpression

-- -- | 6.5.14
-- logicalOrExpression :: MonadPSPArsec s m => m Expression
-- logicalOrExpression =
--   try logicalAndExpression
--   <|> orOp LogicalOr OrP
--   where
--     orOp = binOpExpression logicalOrExpression logicalAndExpression

-- -- | 6.5.15
-- conditionalExpression :: MonadPSPArsec s m => m Expression
-- conditionalExpression =
--   try logicalOrExpression
--   <|> ternaryOp
--   where
--     ternaryOp = do
--       boolExpr <- logicalOrExpression
--       matchToken QuestionMarkP
--       trueCase <- expression
--       matchToken ColonP
--       falseCase <- conditionalExpression
--       pure $ TernaryIfE boolExpr trueCase falseCase

-- -- | 6.5.16
-- assignmentExpression :: MonadPSParsec s m => m Expression
-- assignmentExpression =
--   try conditionalExpression
--   <|> assign
--   where
--     opAssocs = [(Assign, AssingP)
--                ,(MultilyAssign, MultilyAssignP)
--                ,(DivideAssign, DivideAssignP)
--                ,(RemainderAssign, RemainderAssignP)
--                ,(PlusAssign, PlusAssignP)
--                ,(MinusAssign, MinusAssignP)
--                ,(LeftShiftAssign, LeftShiftAssignP)
--                ,(RightShiftAssign, RightShiftAssignP)
--                ,(BinaryAndAssign, BinaryAndAssignP)
--                ,(BinaryXorAssign, BinaryXorAssignP)
--                ,(BinaryOrAssign, BinaryOrAssignP)
--                ]
--     assign = do
--       assignTarget <- conditionalExpression
--       assignOp <- mapM (\ (expr, token) -> try (matchToken token) *> pure expr ) opAssocs
--       assignValue <- assignmentExpression
--       pure $ BinaryOp assignOp assignTarget assignValue

-- -- | 6.5.17
-- expression :: MonadPSParsec s m => m Expression
-- expression =
--   try assignmentExpresion
--   <|> commaSep
--   where
--     commaSep = do
--       e <- expresCommaSeparatedE
--       matchToken CommaP
--       ae <- assignmentExpression
--       pure $ CommaSeparatedE e ae

-- -- | 6.6
-- constantExprssion :: MonadPSParsec s m => m Expression
-- constantExprssion = conditionalExpression

-- -- DECLARATIONS
-- declaration :: MonadPSParsec s m => m Declaration
-- declaration =
--   chose [ do ds <- declarationSpecifiers
--              idl <- initDeclaratorList
--              matchToken SemiColonP
--              pure $ Declaration  ds idl
--         , static_assertDelcaration
--         ]

-- delcarationSpecifiers :: MonadPSParsc s m => m [DeclarationSpecifier]
-- delcarationSpecifiers =
--   chose [ StoragClassSpecifierDS <$> storageClassSpecifier
--         , TypeSpecifierDS <$> typeSpecifier
--         , TypeQualifierDS <$> typeQualifier
--         , FunctionSpecifierDS <$> functionSpecifir
--         , AlignmentSpecifierDS <$> alignmentSpecifier
--         ]

-- initDeclaratorList :: MonadPSParsec s m => m [(Declarator, Maybe Initializer)]
-- initDeclaratorList = do
--   id <- initDeclarator
--   foundComma <- isJust <$> optinal (try (matchToken CommaP))
--   if foundComma
--     then (id:) <$> initDeclaratorList
--     else pure [id]

-- initDeclarator :: MonadPSParsec s m => m (Declarator, Maybe Initializer)
-- initDeclarator = do
--   d <- declarator
--   (try (matchToken EqualP) *> ((d,) . Just) <$> intitializer) <|> (chose (d,Nothing))

-- matchAndGive t res = matchToken t >> pure res

-- storageClassSpecifier :: MonadPSParsec s m => m StorageClassSpecifier
--   chose [ matchAndGive TypedefKW TypeDef
--         , matchAndGive ExternKW Extern
--         , matchAndGive StaticKW Static
--         , matchAndGive ThreadLocalKW TheadLocal
--         , matchAndGive AutoKW Auto
--         , matchAndGive RegisterKW Register
--         ]

-- typeSpecifier :: MonadPSParsec s m => m TypeSpecifier
-- typeSpecifier =
--   chose [ matchAndGive VoidKW Void
--         , matchAndGive CharKW Char
--         , matchAndGive ShortKW Short
--         , matchAndGive IntKW Int
--         , matchToken LongKW Long
--         , matchToken FloatKW Float
--         , matchToken DoubleKW Double
--         , matchToken SignedKW Signed
--         , matchToken UnsignedKW Unsigned
--         , matchToken BoolKW Bool
--         , matchToken ComplexKW Complex
--         , atomicTypeSpecifier
--         , structOrUnionSpecifier
--         , enumSpecifier
--         , typedefName
--         ]

-- structOrUnionSpecifier :: MonadPSParsec s m => m TypeSpecifier
-- structOrUnionSpecifier =
--   try structSpecifier <|> unionSpecifier
--   where
--     unionSpecifier = do
--       matchToken UnionKW
--       try (literal LiteralUnion) <|> named NamedUnion
--     structSpecifier = do
--       matchToken StructKW
--       try (literal LiteralStruct) <|> named NamedStruct

--     literal c = do
--       id <- optional (try identifier)
--       matchToken LeftCurlyBracketP
--       sdl <- structDeclarationList
--       matchToken RightCurlyBracketP
--       pure $ c id sdl

--     named c = c identifier

-- structDeclarationList :: MonadPSParsec s m => m [StructDeclaration]
-- structDeclarationList = some (try structDeclaration)

-- structDeclaration :: MonadPSParsec s m => m StructDeclaration
-- structDeclaration =
--   chose [ StructDeclaration <$>
--           specifierQualifierList  <*> many structDeclarator <* matchToken SimiColonP
--         , StructStaticAsseret <$> static_assertDeclaration
--         ]

-- specifierQualifierList :: MonadPSParsec s m => m [SpecifierQualifier]
-- specifierQualifierList =
--   many (try (TypeSpecifier <$> typeSpecifier)
--         <|> (TypeQualifier <$> typeQualifier))

-- structDeclaratorList :: MonadPSParsec s m => m [StructDeclaration]
-- structDeclaratorList =
--   someSeparatedBy structDeclarator (matchToken CommaP)


-- structDeclarator :: MonadPSParsec s m => m StructDeclaration
-- structDeclarator = do
--   chose [ StructStaticAssert <$> try declarator
--         , undefined
--         ]


-- {-
-- data StructDeclaration where
--   StructStaticAsseret :: [Declarator] ->  StructDeclaration
--   StructDeclaration :: [Declarator] -> Expression ->  StructDeclaration
--   deriving (Show, Read, Eq)
-- -}
