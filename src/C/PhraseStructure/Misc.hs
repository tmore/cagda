-- | Author: Tomas Möre 2019
--
-- This module contains some methods that may be usef
module C.PhraseStructure.Misc where

import C.PhraseStructure.Data

-- | Checks wether an expression is contant or not according to the rules:
--
-- Constant expressions shall not contain assignment, increment, decrement,
-- function-call, or comma operators, except when they are contained within a sub-- expression that is not evaluated. 115)
--
-- Each constant expression shall evaluate to a constant that is in the range of -- representable values for its type
isConstantExpression :: Expression a i -> Bool
isConstantExpression _ = undefined

-- | Checks if a statement is a label
isLableledStatement :: Statement a i -> Bool
isLableledStatement LabeledStm{} = True
isLableledStatement _ = False

-- | Checks if a statment is a selection statement
isSelectionStatement :: Statement a i -> Bool
isSelectionStatement IfStm{} = True
-- isSelectionStatement (IfElseStm _ _ _ _) = True
isSelectionStatement SwitchStm{} = True
isSelectionStatement _ = False

-- | Checks if a statement is an iteration statement
isIterationStatement :: Statement a i -> Bool
isIterationStatement WhileStm{} = True
isIterationStatement DoWhileStm{} = True
isIterationStatement ForExprStm{} = True
isIterationStatement ForDeclStm{} = True
isIterationStatement _ = False
