-- | Author: Tomas Möre 2019
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

{-# LANGUAGE TupleSections #-}
module C.Utils where

import qualified Data.Char as Char
import Text.Megaparsec hiding (Token)
import Data.String

import Data.Foldable

isHexaDec :: String -> Bool
isHexaDec ('0':'x': tail)
  = all Char.isHexDigit tail
isHexaDec _ = False

parseHexaDec :: Integral n => String -> Maybe n
parseHexaDec ('0':'x': tail) = do
  nums <- mapM toNum tail
  pure $ foldr' (\ c acc -> 16 * acc + c) 0 nums
  where
    toNum c =
      if Char.isHexDigit c
      then Just $ fromIntegral $ Char.digitToInt c
      else fail "invalid hexadec"
parseHexaDec  _ = Nothing

isOctal :: Char -> Bool
isOctal c = '0' >= c && c <= '7'

parseOctal :: Integral n => String -> Maybe n
parseOctal ('0':tail) = do
  nums <- mapM toNum tail
  pure $ foldr' (\ c acc -> 8 * acc  + c) 0 nums
  where
    toNum c = if isOctal c
              then Just $ fromIntegral $  fromEnum c - fromEnum '0'
              else Nothing

-- | Gives the max bound for a value as an integer given a TypApplicaton
--
-- > maxVal @Int
-- 9223372036854775807
maxVal :: forall a .  (Integral a, Bounded a) => Integer
maxVal = fromIntegral (maxBound @a)

-- | See maxval
minVal :: forall a .  (Integral a, Bounded a) => Integer
minVal = fromIntegral (minBound @a)


--  5.2.1 Character sets
sourceCharacterSet :: (IsString s, Monoid s) => s
sourceCharacterSet = fromString $
  mconcat [ ['a' .. 'z']
          , ['A' .. 'Z']
          , ['0'..'9']
          , "!\"#%&'()*+,-./:;<=>?[\\]^_{|}~"
          ]

-- | Converts a ParsecT to a ReadS,
parsecToReadS :: Parsec () String a -> ReadS a
parsecToReadS p str =
  either (const []) pure res
  where
    res = parse (p >>= (\a -> (a,) <$> getInput)) "" str

-- | if boolean is ture it capitalizes the character, otherwise not
condCapitalize :: Bool -> Char -> Char
condCapitalize True = Char.toUpper
condCapitalize _ = id


-- | Convert a character into any arbirary stringlike
charToString :: IsString s => Char -> s
charToString = fromString . pure
