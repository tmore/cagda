-- | Author: Tomas Möre 2019
--
-- This module defines some general parsers usefull in other placesq
{-# LANGUAGE GADTs #-}
module ParserUtils where


import Text.Megaparsec

-- | Utility function to see if something parses if not nothing is consumed
doesParse :: (Stream s, Ord e, MonadParsec e s m) => m a -> m Bool
doesParse p = (try p >> pure True) <|> pure False

-- | Parses a sequence of elements separated by a separatur until the separator isn't found anymore
someSeparatedBy :: (Stream s, Ord e, MonadParsec e s m) => m a -> m b -> m [a]
someSeparatedBy p sep = do
  elem <- p
  foundSep <- doesParse sep
  if foundSep
    then (elem:) <$> someSeparatedBy p sep
    else pure [elem]

manySeparatedBy :: (Stream s, Ord e, MonadParsec e s m) => m a -> m b -> m [a]
manySeparatedBy p sep = do
  mElem <- optional (try p)
  case mElem of
    Nothing -> pure []
    Just elem -> do
      foundSep <- doesParse sep
      if foundSep
        then (elem:) <$> someSeparatedBy p sep
        else pure [elem]
